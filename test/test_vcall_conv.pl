#!/usr/bin/perl
use warnings;

my @vcall_list = ("ORF1ab: A1306S,P2046L,P2287S,P4715L,G5063S,P5401L,A6319V;  S: T19R,Phe168_Glu169del,F464C,A626G,I693S,L962I;  ORF8: G8V;",
		"ORF1ab: K261N,A1306S,P2046L,P2287S,V2930L,T3255I;  S: T19R,Phe168_Glu169del,F464C,F490Y,A626G,I693S;  ORF3a: Q38L;  ORF7a: L96P;  ORF8: G8V; N: P6_Asn8delinsH,Thr16fs,E231V,T393I",
		"N: ; ORF1ab: ; ORF3a: ; ORF7a: ; ORF7b: ; ORF8: ; S: ",
		"N: ; ORF1ab: C4543Y; ORF3a: ; ORF7a: P45L; ORF7b: ; ORF8: ; S: T19R");



foreach $vcall(@vcall_list){
	
	my %vcall_hash = vcall_aa_str_tohash(get_onelettercodes($vcall));
	my $vcall_str = vcall_aa_hash_tostr(\%vcall_hash);
	
	print "\n";
	print "orig vcall: \'$vcall\'\n";
	print "conv vcall: \'$vcall_str\'\n\n";

}


# my %gene_variant_pos = vcall_aa_str_tohash("gene1: A10R,A20N; gene2: E10Q,E20G")
#
# Parses vcall for aminoacids to a hash struct
# Example usage:
# $gene_variant_pos->{$gene}->{$variant} # retrieves position for $variant in $gene
#
sub vcall_aa_str_tohash{
	my $vcall_aa_str = shift(@_);
	
	my @gene_varstr_list = split(/;/,$vcall_aa_str);
	my %gene_variant_pos = ();
	foreach my $gene_varstr(@gene_varstr_list){
		$gene_varstr =~ s/^\s+|\s+$//g;
		my ($gene,$varstr) = split(/:/,$gene_varstr,2);
		$varstr =~ s/^\s+|\s+$//g;
		my @variant_list =  split(/,/,$varstr);
		my %variant_pos	 = ();
		foreach my $var(@variant_list){
			$var =~ m/[A-Z]+([0-9]+)/gi;
			my $pos = $1;
			$variant_pos{$var} = $pos;
		}
		$gene_variant_pos{$gene} = \%variant_pos;
	}
	return %gene_variant_pos;
}

# my $vcall_str = vcall_aa_hash_tostr(\%gene_variant_pos)
#
sub vcall_aa_hash_tostr{
	my %gene_variant_pos = %{shift(@_)};
	my @variant_str_list = ();
	foreach my $gene(sort keys %gene_variant_pos){
		my %variant_pos = %{$gene_variant_pos{$gene}};
		if(scalar(%variant_pos) == 0){next;}
		my $variant_str = "$gene: ". join(",",sort {$variant_pos{$a} <=> $variant_pos{$b}} keys %variant_pos);
		push(@variant_str_list,$variant_str);
	}
	return join("; ",@variant_str_list);
}

# my $HGVS_short = get_onelettercodes($HGVS)
#
# Converts 3-letter aa-codes in variant string (HGVS format) to 1-letter aa-codes
# 
sub get_onelettercodes{

my %map_tri_mono_hash = (
	'ALA' => 'A',
	'ARG' => 'R',
	'ASN' => 'N',
	'ASP' => 'D',
	'CYS' => 'C',
	'GLU' => 'E',
	'GLN' => 'Q',
	'GLY' => 'G',
	'HIS' => 'H',
	'ILE' => 'I',
	'LEU' => 'L',
	'LYS' => 'K',
	'MET' => 'M',
	'PHE' => 'F',
	'PRO' => 'P',
	'SER' => 'S',
	'THR' => 'T',
	'TRP' => 'W',
	'TYR' => 'Y',
	'VAL' => 'V');

	my $HGVS = shift(@_);
	
	my $aa_pref = uc(substr($HGVS,0,3));
	my $aa_suff = uc(substr($HGVS,-3,3));
	
	# case1: substitution, will have aa-prefix and suffix: eg ALA123ARG
	if( defined($map_tri_mono_hash{$aa_pref})  && defined($map_tri_mono_hash{$aa_suff}) ) {
		substr($HGVS,0,3,$map_tri_mono_hash{$aa_pref});
		substr($HGVS,-3,3,$map_tri_mono_hash{$aa_suff});
	}
	# case2: deletions, insertion and other calls: screen for aa triple-codes
	else{
		for(my $pos= 0; $pos <=(length($HGVS)-3); $pos++){
			my $tri = uc(substr($HGVS,$pos,3));
			if( defined( $map_tri_mono_hash{$tri} ) ){
				substr($HGVS,$pos,3,$map_tri_mono_hash{$tri});
			}
		}
	}
	return $HGVS;
}
