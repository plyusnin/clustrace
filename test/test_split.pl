#!/usr/bin/perl
use warnings;

my $filein = $ARGV[0];
open (IN,"<$filein") or die "Failed to open $filein: $!\n";

my $i= 0;
while(my $l=<IN>){
	$i++;
	chomp($l); $l =~s/^\s*//;
	my @sp= split(/\t+/,$l,-1);
	
	print "i\t:",join("@",@sp),"\n";
}
