#!/usr/bin/perl
use warnings;
use Getopt::Long qw(GetOptions);

my $usage= 	"\nUSAGE: $0 --gapth num --lenth num --chr str file.msa 1> gaps.bed\n\n";

my $gap_th = 25;
my $len_th = 10;
my $chr	= "Chr1";
GetOptions('gapth=i'=> $gap_th, 'lenth=i' => \$len_th, 'chr=s' => \$chr) or die $usage;
if(scalar(@ARGV)<1){ die $usage;}

print_gaps2bed($ARGV[0],$chr,$gap_th, $len_th);




# print_gaps2bed($file.msa, $chr, $gap_th, $len_th) 1> $file.bed
#
# Creates a BED file representing gaps in the input MSA
#
# $file.msa   Input MSA file
# $file.bed   Output BED file
# $chr        Value for the Chromosome field in the BED file
# $gap_th     Gap threshod between 0 and 100. Include positions with gap% >= gap_th
# $len_th     Gap length threshold. Only output gaps with nt length >= $len_th
#
# DEPENDENCIES: trimal
#
sub print_gaps2bed{
	my $msa 	= shift;
	my $bed		= shift;
	my $chr	   	= shift;
	my $gap_th 	= shift;
	my $len_th	= shift;
	
	
	system("trimal -in $msa -out $msa.tmp  -sgc | tail -n+4 1> $msa.gaps"); 	# ignoring first 3 rows

	# filter with gap_th
	open (IN,"<$msa.gaps") or die "Failed to open $msa.gaps: $!\n";
	open (OUT,">$msa.gaps.flt") or die "Failed to ope $msa.gaps.flt: $!\n";
	while(my $l=<IN>){
		chomp($l);
		$l =~ s/^\s*//; 
		my @sp = split(/\t+/,$l,3);
		if($sp[1] >= $gap_th){
			print OUT join("\t",@sp),"\n";
		}
	}
	close(IN);close(OUT);
	
	open (IN,"<$msa.gaps.flt") or die "Failed to open $msa.gaps.flt: $!\n";
	open (OUT,">$bed") or die "Failed to ope $bed: $!\n";
	my $l=<IN>;
	chomp($l);
	my @sp = split(/\t+/,$l,3);
	my $pos_start	= $sp[0];
	my $pos_end	= $pos_start;
	my $pos		= -1;
	while(my $l=<IN>){
		chomp($l); $l =~ s/^\s*//; 
		@sp = split(/\t+/,$l,2);
		my $pos = $sp[0];
		if($pos > ($pos_end+1)){
			if( ($pos_end-$pos_start+1)>=$len_th){
			print OUT "$chr\t$pos_start\t".($pos_end+1)."\n";
			}
			$pos_start= $pos;
			$pos_end = $pos;
		}
		else{
			$pos_end = $pos;
		}
	}
	if( ($pos_end-$pos_start+1)>=$len_th){
		print OUT "$chr\t$pos_start\t".($pos_end+1)."\n";
	}
	close(IN);
	close(OUT)
	system("rm -f $msa.gaps");
	system("rm -f $msa.gaps.flt");
}
