#!/usr/bin/perl
use strict;
use warnings;
use Excel::Writer::XLSX;


# Converts input files to a multisheet Excel file

my $usage=      "USAGE: $0 table1.tab table2.tab table3.tab > table1-2-3.xlsx\n\n";
		
if(scalar @ARGV < 1){ die $usage; }


# Create a new Excel workbook
binmode(STDOUT);
my $workbook = Excel::Writer::XLSX->new( \*STDOUT );


foreach my $file(@ARGV){
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $label= $file;
	my @tmp= split(/\//,$label);	# remove /*/* prefix
	$label = $tmp[$#tmp];
	$label =~ s/\.\w+$//g;		# remove .* suffix
	
	
	# Add a worksheet
	my $worksheet = $workbook->add_worksheet($label);
	#my $format = $workbook->add_format();
		
	my $rowi = 0;
 	while( my $l=<IN> ){
		#if( $l =~ m/^[\!#]/){next;}
		chomp($l);
		my @sp = split(/\t/,$l,-1);
		for(my $coli=0; $coli<scalar @sp; $coli++){
			$worksheet->write($rowi,$coli,$sp[$coli]);
		}
		$rowi++;
	}
	close(IN);
}



