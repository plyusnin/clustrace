#! /usr/bin/perl
use strict;
use warnings;

use Cwd;
use Bio::TreeIO;
use Clone 'clone';
use Excel::Writer::XLSX;
use File::Basename;
use File::Temp  qw(tempdir);
use Getopt::Long qw(GetOptions);
use Sort::Naturally;
use sort 'stable';
use Struct::Dumb;
use Time::Piece;


struct AAVariant => [qw( ntpos gene ref alt aapos HGVSp HGVSc mstr count freq )], named_constructor => 1;
struct NTVariant => [qw( ntpos gene ref alt mstr count freq )], named_constructor =>1;


#
# SEQUENCE ANALYSIS FOR TRACING COVID-19 TRANSMISSION CHAINS
#
# credit: Ilya Plyusnin, University of Helsinki, Ilja.Pljusnin@helsinki.fi
#
# DEPENDENCIES:
# 	mafft
# 	iqtree2
# 	seqkit
# 	vcftools
# 	msa2vcf.jar
# 	snpEff.jar

my $usage= 	"USAGE: $0 --fasta file --res dir --pipe str [--target str --minseqn int --minlen int --maxlen int --maxgap int -t|numth int --tree str --ufboot --col str --short -v]\n".
		"\n".
		"--fasta file          : Input multifasta (*.fa or *.fasta)\n".
		"--res dir             : Output directory. Default: results\n".
		"--log dir             : Direcotry for logging. Default: log\n".
		"--colpan str          : Color panel for coloring clusters: rgb|paired|dark2 [dark2](for preview see https://colorbrewer2.org/)\n".
		"--minseqn int         : Lineage filtering: exclude lineages with seqn<minseqn [10]\n".
		"--minlen int          : Sequence outlier filtering: exclude seqs shorted than median_length*minlen% [90]\n".
		"--maxlen int          : Sequence outlier filtering: exclude seqs longer than median_length*maxlen% [110]\n".
		"--maxgap int          : Sequence outlier filtering: exclude seqs with gaps% > maxgap% [10]\n".
		"--tree str            : Run iqtree (IQ-Tree2 --mset GTR+F) or vftree (VeryFastTree --gtr -nt) [iqtree]\n".
		"--ufboot              : Run iqtree with ultrafast bootstrap and create consensus tree (iqtree -B 1000 -bnni --date TAXNAME) [false]\n".
		"--trimal_gt int       : trimal -gt threshold. Used to trim MSAs before tree construction. [0.9]\n".
		"--tperiod str         : Time period for cluster analysis. Accepted values: month|week [month]\n".
		"--outgroup file       : Fasta with ourgroup sequence. Default: data/NC_045512.fa\n".
		"--refgen file         : Fasta with refenrece genome. Default: data/NC_045512.fa\n".
		"--refvar file         : File with reference lineage variants. Format:\n".
		"                        lineageid \\t gene1: var1,var2,..[,varn]; gene2: var1,var2,..[,varn] \\n\n".
		"--pipe str            : Comma-separated list of steps to perform, eg --pipe c,f,a [all]\n".
		"    p|pangolin        : Assign lineages with Pangolin\n".
		"    c|collect         : Collect seqs for each lineage into multi-fasta. WARNING: this will cleanup res-dir\n".
		"    f|filter          : Filter multi-fasta. Applied to all res directories\n".
		"    a|align           : Create MSA(s). In reference mode will add fasta in --res dir to MSA in --ref dir.\n".
		"    t|tree            : Create tree(s). In reference mode will constrain all trees by the lineage.ml.tree in --ref dir.\n".
		"                        Note: to create constrained trees use iqtree.\n".
		"    cl|clust          : Extract clusters at various mutation rates. In reference mode will match clusters between --res and --ref dirs.\n".
		"    vc|vcall          : Create MSA and VCF files for all clusters. Add vcf variants to cluster summary excel.\n".
		"    vclineage         : Create VCF files for all lineage MSA(s) in --res dir. Create excel summary with VCF variants for each lineage.\n".
		"    pack              : Pack results into a tarball. Tarball will be created to the root directory of --res dir.\n".
		"    clean             : Clean up space by removing all intermediate and temporary files.\n".
		"    all               : Run all steps [default]\n".
		"--target str          : Comma-separated list of target lineages to analize (eg --target B.1.1.7). All other lineages will be ignored [false]\n".
		"-t|numth int          : Number of threads [8]\n".
		"--short               : Truncate seq names to the first occurance of '_' [false]\n".
		"--keep_wrkdir         : Do not delete wkrdir after finishing\n".
		"-v                    : Verbal mode [false]\n\n";


# INIT DEFAULT AND COMMAND LINE OPTIONS

my %opt 						= init_opt();
my %pipeh						= %{$opt{'pipeh'}};
my @clustering_mutation_rates	= (30.0,20.0);
my @clustering_thresholds		= ();
foreach my $mutation_rate(@clustering_mutation_rates){
	my $threshold				=  $mutation_rate /$opt{'refgen_len'};
	push(@clustering_thresholds,$threshold);
}

# CREATE WRKDIR
if( $opt{'keep_wrkdir'} ){
	$opt{'wrkdir'} 	= tempdir("clustrace_wrkdir_XXXXXXXX",CLEANUP => 0);
}
else{
	$opt{'wrkdir'} 	= tempdir("clustrace_wrkdir_XXXXXXXX",CLEANUP => 1);
}
system("mkdir -p $opt{'log'}");
system("mkdir -p $opt{'res'}");

## START PIPELINE
if($pipeh{'pangolin'}){
	print STDERR "\n\e[4m RUN PANGOLIN \e[0m\n\n";
	
	# in 
	my $fasta		= $opt{'fasta'};
	# out
	my $outfile 	= $opt{'lineage_report'};
	my $log 		= "$opt{'log'}/pangolin.log";
	
	system_call("$opt{'pangolin_call'} $fasta --outfile $outfile -t $opt{'numth'} --tempdir $opt{'wrkdir'} &> $log", $opt{'v'});
}

if($pipeh{'collect'}){
  	print STDERR "\n\e[4m COLLECT LINEAGE FASTA(S) \e[0m\n\n";
	pipe_collect(\%opt);
}


if( $pipeh{'filter'} ){
	print STDERR "\n\e[4m FILTERING OUTLIERS \e[0m\n\n";
	pipe_filter(\%opt);
}


if( $pipeh{'align'} ){
	print STDERR "\n\e[4m CREATE/UPDATE MSA(S) \e[0m\n\n";
	pipe_align(\%opt);
}


if( $pipeh{'tree'} ){
	print STDERR "\n\e[4m CREATE PHYLO TREE(S) \e[0m\n\n";
	pipe_tree(\%opt);
}


if( $pipeh{'clust'} ){
	print STDERR "\n\e[4m EXTRACT CLUSTERS \e[0m\n\n";
	pipe_clusters(\%opt);
	write_cluster_summary(\%opt,!1);
}


if( $pipeh{'vcall'} ){
	print STDERR "\n\e[4m CLUSTER MSA(S) + VCF(S)\e[0m\n\n";
	
	write_cluster_msas(\%opt);
	write_cluster_vcfs(\%opt);
	write_cluster_summary(\%opt,1); # Rewrite cluster summary including vcf summary
}

if( $pipeh{'vclineage'} ){
	print STDERR "\n\e[4m LINEAGE VCF(S)\e[0m\n\n";
	
	write_lineage_vcfs(\%opt);
	write_lineage_summary(\%opt);
}


if( $pipeh{'clean'} ){
	print STDERR "\n\e[4m CLEANUP TEMP FILES \e[0m\n\n";
	
	system_call( "rm -f $opt{'res'}/*.tmp", $opt{'v'} );			# All temporary files: THIS CAN POTENTIALLY KILL OTHER JOBS RUNNIGN ON THE SAME DATA!
	system_call( "rm -f $opt{'res'}/*.seqkit.fai", $opt{'v'} );	# Seqkit index files
	system_call( "rm -f $opt{'res'}/*.ckp.gz", $opt{'v'} );		# IQtree heckpoint files
	system_call( "rm -f $opt{'res'}/*.ufboot", $opt{'v'} );			# Uncomment to remove ufboot trees
}

if( $pipeh{'pack'} ){
	print STDERR "\n\e[4m PACK FILES FOR SHARING \e[0m\n\n";
	
	my $dirname	= dirname($opt{'res'});
	my $basename 	= basename($opt{'res'});

	system_call( "rm -fR $dirname/$basename.tar", $opt{'v'} );
	system_call( "mkdir -p $dirname/$basename.tar", $opt{'v'} );		

	my @files_share		= ();
	push(@files_share, <$opt{'res'}/*.fasta>);
	push(@files_share, <$opt{'res'}/*.fasta.flt>);
	push(@files_share, <$opt{'res'}/*.fasta.stats>);
	push(@files_share, <$opt{'res'}/*.msa>);
	push(@files_share, <$opt{'res'}/*.msa.withrefseq>);
	push(@files_share, <$opt{'res'}/*.vcf>);
	push(@files_share, <$opt{'res'}/*.tree>);
	push(@files_share, <$opt{'res'}/*tree*.nex>);
	push(@files_share, <$opt{'res'}/*tseries.nex>);
	push(@files_share, <$opt{'res'}/*.cl>);
	push(@files_share, <$opt{'res'}/*.xlsx>);
	push(@files_share, <$opt{'res'}/*.jpg>);
	push(@files_share, <$opt{'res'}/*/>);
	#push(@files_share, <$opt{'res'}/*cluster_summary.tab>);

	my $files_share_str = join(" ",@files_share);
	system("cp -r $files_share_str $dirname/$basename.tar/");
	system_call( "tar -czf $dirname/$basename.tar.gz -C $dirname/$basename.tar .", $opt{'v'} );
	system_call( "rm -fR $dirname/$basename.tar", $opt{'v'} );
}

if(!$opt{'keep_wrkdir'}){	# PROBABLY REDUNDANT
	system_call( "rm -fR $opt{'wrkdir'}", $opt{'v'} );
}


# COLLECT LINEAGE SEQUENCES TO MULTIFASTA
sub pipe_collect{
	
	# in
	my $fasta			= $opt{'fasta'};
	my $lineage_report	= $opt{'lineage_report'};
	
	# out: 
	# mulfifasta for each lineage
	
	
	# read lineage_report
	my %seqid_lineage	= ();
	my %header_musthave	= ("taxon"=>1,"lineage"=>1);
	my %header_col		= ();

		# read header
	if($opt{'v'}){
		print STDERR "\treading $lineage_report..\n";
	}
	open(IN,"<$lineage_report") or die "Can\'t open $lineage_report: $!\n";
	my $ln 	= 0;
	my $l	= <IN>;
	chomp($l);
	$l	=~ s/^#//;
	my @sp	= split(/,/,$l,-1);	
	for(my $col=0; $col<scalar(@sp); $col++){
		if(defined($header_musthave{$sp[$col]})){
			$header_col{$sp[$col]} = $col;
	}}
	foreach my $h(keys %header_musthave){
		if( !defined($header_col{$h}) ){
			print STDERR "\tERROR: $0: $opt{'lineage_report'} has no header $h: exiting\n";
			exit(1);
	}}	
		# read data
	while(my $l=<IN>){
        $ln++;
		if($l =~ m/^[@#!]/){ next; }
		chomp($l);
		my @sp= split(/,/,$l,-1);
		my $seqid 	= $sp[$header_col{'taxon'}];
		my $lineage	= $sp[$header_col{'lineage'}];
		$seqid_lineage{$seqid} = $lineage;
	}

	# read multifasta
	if($opt{'v'}){ print STDERR "\treading $fasta\n"; }
	my %seqhash 	= readfasta($fasta);
	my $count		= 0;
	my %lineage_seqhash = ();

	foreach my $seqid(sort keys %seqhash){

		if( !defined($seqid_lineage{$seqid})){
			print STDERR "\tWARNING: no mapping for $seqid: skipping\n";
			next;
		}
		my $lineage	= $seqid_lineage{$seqid};
	
		# init hash
		if( !defined($lineage_seqhash{$lineage}) ){
			my %tmp=();
			$lineage_seqhash{$lineage} = \%tmp;
		}
		$lineage_seqhash{$lineage}->{$seqid} = 	$seqhash{$seqid};
		$count++;
	}

		# split multifasta by lineage, limit to target lineages
	foreach my $lineage(sort keys %lineage_seqhash){
		
		if($opt{'target'}){ # LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineage}) ){
				next;
		}}
		
		my $seqn  = scalar(keys %{$lineage_seqhash{$lineage}});
		if($opt{'minseqn'}  &&  ($seqn<$opt{'minseqn'}) ){  
			print STDERR "\tskipping $lineage: seqn=$seqn < $opt{'minseqn'}\n";
			next;
		}	
	
		if($opt{'v'}){ print STDERR "\twriting $seqn sequences to $opt{'res'}/$lineage.fasta\n";}
		my $fasta_out = "$opt{'res'}/$lineage.fasta";
		open(OUT,">$fasta_out") or die "Can\'t open $fasta_out: $!\n";
		foreach my $seqid( sort keys %{$lineage_seqhash{$lineage}} ){
			my $seq = $lineage_seqhash{$lineage}->{$seqid};
			print OUT ">$seqid\n";
			print OUT "$seq\n";
		}
		close(OUT);
	}
}


# FILTER OUTLIER SEQUENCES, PRINT STATS
# pipe_filter(\%opt,$dir)
#
# --pipe filter step
sub pipe_filter{
	
	my %opt			= %{shift(@_)};
	my @fasta_list  = ();
	push(@fasta_list, <$opt{'res'}/*.fasta>);

	foreach my $fasta(@fasta_list){
		system_call( "seqkit fx2tab -B N -l -ni $fasta 1> $fasta.stats", $opt{'v'});
		my %length_hash	= ();
		my %Ncont_hash	= ();	
		read_tohash("$fasta.stats",0,1,\%length_hash);
		read_tohash("$fasta.stats",0,2,\%Ncont_hash);
		my @length_list 	= values %length_hash;
		my $median_length 	= median(\@length_list);
		my $min_length		= int($median_length * $opt{'minlen'}/100.0);
		my $max_length		= int($median_length * $opt{'maxlen'}/100.0);
		my $flt_count		= 0;
		my $sel_count		= 0;

		open(SEL,">$fasta.sel.tmp") or die "Can\'t open $fasta.sel.tmp: $!\n";
		open(FLT,">$fasta.flt.tmp") or die "Cant open $fasta.flt.tmp: $!\n";
		open(STATS,">$fasta.stats") or die "Cant open $fasta.stats: $!\n";
		print STATS "#seqid\tlength\tNcontent\tfilter\n";

		foreach my $name(keys %length_hash){
			my $length	= $length_hash{$name};
			my $Ncont 	= $Ncont_hash{$name};
			if( $length < $min_length ){
				print FLT "$name\n";
				print STATS "$name\t$length_hash{$name}\t$Ncont_hash{$name}\tseqlen<$opt{'minlen'}%*median\n";
				$flt_count++;
			}
			elsif($length > $max_length){
				print FLT "$name\n";
				print STATS "$name\t$length_hash{$name}\t$Ncont_hash{$name}\tseqlen>$opt{'maxlen'}%*median\n";
				$flt_count++;	
			}
			elsif($Ncont > $opt{'maxgap'}){
			print FLT "$name\n";
			print STATS "$name\t$length_hash{$name}\t$Ncont_hash{$name}\tgaps>$opt{'maxgap'}\n";
			$flt_count++;
			}
			else{
				print SEL "$name\n";
				print STATS "$name\t$length_hash{$name}\t$Ncont_hash{$name}\tpassed\n";
				$sel_count++;
			}
		}
		close(SEL); close(FLT); close(STATS);		
		if($sel_count == 0){
			system_call( "mv $fasta $fasta.flt", $opt{'v'} );
			print STDERR "\tOnly outliers. For details see: $fasta.stats\n";
		}
		elsif($flt_count > 0){
			system_call( "seqkit faidx --full-head $fasta --infile-list $fasta.flt.tmp 1> $fasta.flt", $opt{'v'} );
			system_call( "seqkit faidx --full-head $fasta --infile-list $fasta.sel.tmp 1> $fasta.tmp", $opt{'v'} );
			system_call( "mv $fasta.tmp $fasta", $opt{'v'} );
			print STDERR "\tFiltered $flt_count outliers from $fasta to $fasta.flt\n";
			print STDERR "\tFor details see: $fasta.stats\n";
		}
		else{
			print STDERR "\tNo outliers. For details see: $fasta.stats\n";
		}
		system_call( "rm -f $fasta.*.tmp", $opt{'v'} );
	}
}

# CREATE/UPDATE MSA(S)
# pipe_align(\%opt)
#
# --pipe align step
sub pipe_align{
	my %opt		= %{shift(@_)};
	
	my $log 	= "$opt{'log'}/align.log";
	
	# Go through *.fasta in res and construct/update MSAs
	my @fasta_list  = ();
	push(@fasta_list, <$opt{'res'}/*.fasta>);

	foreach my $lineage_fasta(@fasta_list){			
		# in:
		$lineage_fasta					=~ m/\/([^\/]+)\.(fa|fasta)$/;
		
		# out: 
		my $lineageid					= $1;
		my $lineage_fasta_withoutgroup	= "$lineage_fasta.withoutgroup";
		my $lineage_msa 				= "$opt{'res'}/$lineageid.msa";
		my $lineage_msa_withrefseq		= "$opt{'res'}/$lineageid.msa.withrefseq";
		

		if($opt{'target'}){ # LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){
				next;
		}}
		   			
		# add outgroup to lineage fasta, unless its already there
		if( !has_seqid($lineage_fasta, $opt{'outgroup_id'}) ){
			system_call("cat $lineage_fasta $opt{'outgroup'} 1> $lineage_fasta_withoutgroup", $opt{'v'});
		}
		else{
			system_call("cp $lineage_fasta $lineage_fasta_withoutgroup", $opt{'v'});
		}
		
		# align
		system_call("$opt{'msa_call'} --thread $opt{'numth'} $lineage_fasta_withoutgroup 2> $log | ".
					"seqkit seq -i -u -w0 1> $lineage_msa 2>> $log", $opt{'v'});
		
		# add refseq to $lineage_msa_withrefseq (should include both outgroup + refseq)
		if( $opt{'refgen_id'} eq $opt{'outgroup_id'}){
		
			system_call("cp $lineage_msa $lineage_msa_withrefseq",$opt{'v'});
		}
		else{
			system_call("$opt{'msa_call'} --thread $opt{'numth'} --add $opt{'refgen'} $lineage_msa 2>> $log | ".
						"seqkit seq -i -u -w0 1> $lineage_msa_withrefseq 2>> $log", $opt{'v'});
		}
				
	}
}

# CREATE TREES
# pipe_tree(\%opt)
#
sub pipe_tree{
	my %opt			= %{shift(@_)};

	# in:	
	my @msa_list 	= <$opt{'res'}/*.msa>;
	# out:
	# ML or concensus trees for each lineage
	my $log 		= "$opt{'log'}/tree.log";
	

	foreach my $msa(@msa_list){			
		my $prefix		= $msa;
		$prefix 		=~ s/\.(msa)$//;
		$prefix			=~ m/\/([^\/]+)$/;
		my $lineageid	= $1;

		if($opt{'target'}){	# LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){ next;}
		}		
		if(get_seqnum("$msa") < 5){
			print STDERR "\tNOTE: skipping MSA with <5 seqs: $msa\n";
			next;
		}
		
		# In case any duplicates appeard in MSA, rename with *_N
		system_call( "seqkit rename -w0 $msa 1> $msa.tmp 2> $log", $opt{'v'});
		system_call( "mv $msa.tmp $msa", $opt{'v'});

		# REMOVE GAPPY SITES (MOSTLY AT START + END)
		system_call( "seqkit replace -w0 -s -p 'N' -r '-' $msa 1> $msa.tmp 2>> $log", $opt{'v'});
		system_call( "mv $msa.tmp $msa", $opt{'v'});
		system_call( "trimal -in $msa -out $msa.gapflt -fasta -gt $opt{'trimal_gt'} 2>> $log", $opt{'v'});

		if($opt{'tree'} eq 'iqtree'){
	
			system_call("$opt{'iqtree_call'} -ntmax $opt{'numth'} --prefix $prefix -s $msa.gapflt &>> $log", $opt{'v'} );
			
			if(-e "$prefix.treefile"){
				system_call("mv $prefix.treefile $prefix.ml.tree", $opt{'v'});
			}
			if(-e "$prefix.contree"){
				system_call("mv $prefix.contree $prefix.con.tree", $opt{'v'});
			}
		}
		elsif($opt{'tree'} eq 'vftree'){
			
			system_call("$opt{'vftree_call'} -threads $opt{'numth'} $msa.gapflt 1> $prefix.ml.tree 2>> $log", $opt{'v'});
		}
		system_call("rm -f $msa.gapflt", $opt{'v'});


		# REROOT ON OUTGROUP
		my @treefiles 	= ("$prefix.ml.tree","$prefix.con.tree");
		foreach my $treefile(@treefiles){
		  if(-e "$treefile"){
			my $treeio		= Bio::TreeIO->new(-file => "$treefile");
			my $tree_obj		= $treeio->next_tree();
			my $outgroup_node	= $tree_obj->find_node($opt{'outgroup_id'});
			if(!defined($outgroup_node)){	
				$outgroup_node	= $tree_obj->find_node(uc($opt{'outgroup_id'}));}	# some progs will change seqids to uppercase
			if(!defined($outgroup_node)){
				print STDERR "\t\tWARNING: outgroup seqid=$opt{'outgroup_id'} not found in $treefile: skip rerooting\n";
			}
			else{
				my $newroot	= $outgroup_node->ancestor();
				my $rerooted	= !1;
				if( defined($newroot)){
				$rerooted	= $tree_obj->reroot( $newroot );
				}
				if($rerooted){
					open(OUT,">$treefile") or die "Failed to open $treefile: $!\n";
					print OUT get_newick_str( $tree_obj->get_root_node()),";\n";
					close(OUT);
				#print STDERR "rerooted $treefile :)\n";
				}
		}}}
	}
}

# EXTRACT CLUSTERS FOR --RES DIR WITH TREECLUSTER.PY, OR MAP CLUSTERS FROM --REF DIR TO --RES DIR WITH CUSTOM ALGORITHM
# pipe_clusters(\%opt)
#
sub pipe_clusters{
	my %opt		= %{shift(@_)};
	
	# in:
	my @tree_list = ();
	if($opt{'ufboot'}){
		push(@tree_list, <$opt{'res'}/*.con.tree>);
	}
	else{
		push(@tree_list, <$opt{'res'}/*.ml.tree>);
	}
	
	# out:
	# $lineage.mr=$mr.cl
	
	foreach my $tree(@tree_list){
		
		$tree	 		=~ m/\/([^\/]+)\.(ml|con)\.tree$/i;
		my $lineageid	= $1;

		if($opt{'target'}){	# LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){ next;}
		}

		for(my $i=0; $i<scalar(@clustering_thresholds); $i++){
			my $mr	 		= $clustering_mutation_rates[$i];
			my $th			= $clustering_thresholds[$i];
			my $clust_file	= "$opt{'res'}/$lineageid.mr=$mr.cl";
			
			if($opt{'v'}){
			print STDERR "\tExtracting clusters: $clust_file\n";
			}
				
			system_call( "TreeCluster.py -i $tree -m max_clade -t $th | tail -n+2 > $clust_file.tmp", $opt{'v'});
			my @clust_table 	= read_totable("$clust_file.tmp");
			@clust_table 		= sort { ncmp($a->[0],$b->[0]) } @clust_table;	# sort naturally by seqid
			@clust_table		= sort { $a->[1] <=> $b->[1] } @clust_table;	# then by cluster number
			my ($ind0,$ind1)	= (0,0);										# move singletons to the end of the table
			while($clust_table[$ind1]->[1] == -1){ $ind1++;}
			my @singletons		= splice(@clust_table,$ind0,$ind1);
			push(@clust_table,@singletons);				
				
			# GET CLUSTER BRANCH SUPPORT VALUES FROM THE TREE
			my $treeio			= Bio::TreeIO->new(-file   => "$tree");
			my $tree_obj		= $treeio->next_tree();
			my %nodeid_clustid  = ();
			read_tohash("$clust_file.tmp",0,1,\%nodeid_clustid);
			my %clust_lca		= get_cluster_lca($tree_obj, \%nodeid_clustid);
			my %clust_support 	= ();
			foreach my $clust(keys %clust_lca){
				my $lca = $clust_lca{$clust};
				if(defined($lca->ancestor())){
					# IQTree2 will write bootstrap values as ids
					if(defined($lca->ancestor())){
						$clust_support{$clust}= $lca->ancestor()->id(); 
					}
					else{ # root node
						$clust_support{$clust} = 100;
					}
				}
				else{
					$clust_support{$clust}= 'NA';
				}
			}
				
			# ADD SUBCLUSTERN AND BRANCH SUPPORT VALUES TO THE CLUSTER TABLE
			foreach my $row(@clust_table){	# add subclustern which is zero for newly captured clusters
				my $subclustern 	= 0;
				my $support			= defined($clust_support{$row->[1]})? $clust_support{$row->[1]} : 'NA';
				push(@{$row}, $subclustern, $support);
			}
				
			print_table($clust_file,\@clust_table,['#sequence','cluster','subclustern','support']);
			if($opt{'colorclust'}){
				  system_call( "perl tree_colorclust.pl --tree $tree --clust $clust_file --colpan $opt{'colpan_str'} 1> $tree.mr=$mr.nex", $opt{'v'});
			}
			system_call( "rm -f $clust_file.tmp", $opt{'v'});
		}
	}
}


# Creates cluster excel summary file. This includes the following data sheets:
# 	- clustSeqN				: seqn, subclustern and support values for each cluster at each MR and time period
#	- clustSEqID			: seqids, for each cluster at each MR and time period
#	- clustGR_MR=X			: seqn for each cluster at each time period + medianGR + maxGR + support values. Separate sheet for each MR
#	- clustMutations_MR=X	: nt and aa variants for each cluster
# 
sub write_cluster_summary{

	my %opt			= %{shift(@_)};
	my $vcfsummary	= (scalar(@_)>0)? shift(@_): !1;
	
	# Iterate lineageid(s)
	my @tree_list = ();
	push(@tree_list, <$opt{'res'}/*.ml.tree>);
	
	foreach my $tree(@tree_list){		
		$tree	 		=~ m/\/([^\/]+)\.ml\.tree$/g;
		my $lineageid		= $1;
		my $summary_file_excel	= "$opt{'res'}/$lineageid.cluster_summary.xlsx";
		my $lineage_msa		= "$opt{'res'}/$lineageid.msa";
		
		if($opt{'target'} && !defined($opt{'target'}->{$lineageid}) ){ next;}


		# COLLECT DATA for clustSeqN: $mr->$period->$cluster->($seqn,$subclustn,support)
		# COLLECT DATA for clustSeqID: 2D table (list of lists)
		my %mr_period_cl_data 	= ();
		my @data2		= ();
		
		foreach my $mr(@clustering_mutation_rates){
			# init hash
			unless( defined($mr_period_cl_data{$mr}) )	{ my %tmp = ();$mr_period_cl_data{$mr} = \%tmp;}
			my $clust_file	= "$opt{'res'}/$lineageid.mr=$mr.cl";
			my %node_clust	= ();
			my %clust_subcl = ();
			my %clust_supp	=();	
			unless( -e "$clust_file"){ print STDERR "\tWARNING: missing file $clust_file: skipping\n"; next; }
			read_tohash($clust_file,0,1,\%node_clust);
			read_tohash($clust_file,1,2,\%clust_subcl);
			read_tohash($clust_file,1,3,\%clust_supp);
			my @clusters = sort {$a <=> $b} keys %clust_subcl;
			
			# Delete outgroup/refgen from clusters
			if( defined($node_clust{$opt{'refgen_id'}}) ){
				delete($node_clust{$opt{'refgen_id'}});
			}
			if( defined($node_clust{$opt{'outgroup_id'}}) ){
				delete($node_clust{$opt{'outgroup_id'}});
			}
				
			# Assign seqs to date periods
			my %node_period = ();
			foreach my $node(keys %node_clust){
				 my $period = get_period_label($node,$opt{'tperiod'});
				 if($period){
				 	$node_period{$node} = $period;
				 }
			}
			my %period_nodelist = invert_hash(\%node_period);
				
			# sequence counts + seqids for each cluster up to each period
			foreach my $period( reverse(nsort(keys %period_nodelist)) ){
				
				unless( defined($mr_period_cl_data{$mr}->{$period}) ){ my %tmp = ();$mr_period_cl_data{$mr}->{$period} = \%tmp;}
								
				my %clust_seqn  	= get_classcounts(\%node_clust);
				foreach my $clust(@clusters){
					my $seqn	= defined($clust_seqn{$clust})  ? $clust_seqn{$clust} : 0;
					my $subcl 	= defined($clust_subcl{$clust}) ? $clust_subcl{$clust}: 0;
					my $supp	= defined($clust_supp{$clust})  ? $clust_supp{$clust} : 'NA';
					my @tmp 	= ($seqn,$subcl,$supp);
					$mr_period_cl_data{$mr}->{$period}->{$clust} = \@tmp;
				}
				
				my %clust_nodes = invert_hash(\%node_clust);
				
				foreach my $clust( sort {$a <=> $b} keys %clust_nodes){
					my @nodes = nsort( @{$clust_nodes{$clust}} );
					foreach my $seqid(@nodes){
						my @tmp	= ($period,$mr,$clust,$seqid);
						push(@data2,\@tmp);
					}
				}			
				# Now delete seqs for this time period
				delete @node_clust{ @{$period_nodelist{$period}} };
			}
		}
		
		# CREATE TOTAL COUNTS OF SEQS + cluster set
		my %mr_clusterset = ();
		foreach my $mr(keys %mr_period_cl_data){
			my %tmp = ();
			$mr_clusterset{$mr} = \%tmp;
			foreach my $period(keys %{$mr_period_cl_data{$mr}}){
				my $s = 0;
				foreach my $cluster(keys %{$mr_period_cl_data{$mr}->{$period}}){
					$s += $mr_period_cl_data{$mr}->{$period}->{$cluster}->[0];
					$mr_clusterset{$mr}->{$cluster} = 1;
				}
				my @total= ($s,0,0);
				$mr_period_cl_data{$mr}->{$period}->{'total'} = \@total;
			}
		}

		# CALCULATE medianGR + maxGR for each cluster
		my %mr_clust_medGR = ();
		my %mr_clust_maxGR = ();
		foreach my $mr(keys %mr_period_cl_data){
			my %tmp = ();
			my %tmp2= ();
			$mr_clust_medGR{$mr}= \%tmp;
			$mr_clust_maxGR{$mr}= \%tmp2;
			my @periods = sort keys %{$mr_period_cl_data{$mr}};
			#print "DEBUG: ",join(",",@periods,"\n"); exit(1);
			
			my @clusters = sort {$a <=> $b} keys %{$mr_clusterset{$mr}};
			foreach my $cluster( (@clusters,'total') ){
				my @gr_list = ();
				for(my $i=1; $i<scalar(@periods); $i++){
					my $gr = ($mr_period_cl_data{$mr}->{$periods[$i]}->{$cluster}->[0]) - ($mr_period_cl_data{$mr}->{$periods[$i-1]}->{$cluster}->[0]);
					push(@gr_list, $gr);
				}
				$mr_clust_medGR{$mr}->{$cluster}= median(\@gr_list);
				$mr_clust_maxGR{$mr}->{$cluster}= max(\@gr_list);
			}
		}
		
		
		# PARS VCF, EXTRACT unique and novel aa variants
		my %mr_cl_AAV			= ();
		my %mr_cl_NTV			= ();
		my %mr_cl_uniqueAAV 	= ();
		my %mr_cl_nonrefAAV  	= ();
		my %refAAV				= (); # %hash->@array
		my $refAAVstr			= 'NA';
		
		if( $opt{'refvar'} ){
			my %refvars	= ();
			read_tohash($opt{'refvar'},0,1,\%refvars);
			if( defined($refvars{$lineageid}) ){
				%refAAV 	= AAVariantString2AAVariantHash( $refvars{$lineageid} );
				$refAAVstr	= AAVariantHash2AAVariantString(\%refAAV);	# sorts genes
			}
		}

		if($vcfsummary){
		  foreach my $mr(@clustering_mutation_rates){
			# init hash
			my %tmp	= (); $mr_cl_AAV{$mr}		= \%tmp;
			my %tmp1= (); $mr_cl_NTV{$mr}		= \%tmp1;
			my %tmp2= (); $mr_cl_uniqueAAV{$mr} 	= \%tmp2;
			my %tmp3= (); $mr_cl_nonrefAAV{$mr}	= \%tmp3;

			my @cluster_list = sort {$a <=> $b} keys %{$mr_clusterset{$mr}};
			
			# Parse VCF for each cluster
			foreach my $cl(@cluster_list){
				
				my $vcf_file = "$opt{'res'}/$lineageid/mr=$mr/$lineageid.mr=$mr.cl=$cl.vcf";
				
				if( !(-e $vcf_file)){ next; }
				
				my %vdata= parseVariants($vcf_file,
							$opt{'vcall_freq_th_cl'},
							$opt{'vcall_count_th_cl'},
							$opt{'vcall_fromnt'},
							$opt{'vcall_tont'},
							$opt{'vcall_nt_allele_maxlen'});
				
				$mr_cl_AAV{$mr}->{$cl} = $vdata{'bygene2'};
				$mr_cl_NTV{$mr}->{$cl} = $vdata{'nt.bygene'};
				
				#print STDERR "$cl:\n",AAVariantHash2AAVariantString($vdata{'bygene'}),"\n\n";
			}

			# Get unique + nonref + nonref Spike AAVariants 
			foreach my $cluster_a(@cluster_list){		
				if( !defined($mr_cl_AAV{$mr}->{$cluster_a}) ){	next; }
				
				my %AAV_a 		= %{ clone($mr_cl_AAV{$mr}->{$cluster_a}) };
				my %uniqueAAV 	= %{ clone($mr_cl_AAV{$mr}->{$cluster_a}) };
				my %nonrefAAV 	= %{ clone($mr_cl_AAV{$mr}->{$cluster_a}) };
				
				# EXTRACT UNIQUE VARIANTS: remove variants in all other clusters
				foreach my $cluster_b(@cluster_list){
					if($cluster_a eq $cluster_b){next;}
					if( !defined($mr_cl_AAV{$mr}->{$cluster_b})){ next; }

					my %AAV_b 	= %{$mr_cl_AAV{$mr}->{$cluster_b} };

					foreach my $gene(keys %AAV_b){
						if(!defined($uniqueAAV{$gene})){ next;}
						
						foreach my $var( keys ( %{$AAV_b{$gene}} ) ){
							delete( $uniqueAAV{$gene}->{$var});
						}
					}
				}
				$mr_cl_uniqueAAV{$mr}->{$cluster_a} = \%uniqueAAV;

				# EXTRACT NONREF VARIANTS
				if( $refAAVstr ne 'NA' ){
					
					
					# For each gene delete all refvars
					foreach my $gene(keys %refAAV){
						if(defined($nonrefAAV{$gene})){
							foreach my $var( @{$refAAV{$gene}} ){
								delete( $nonrefAAV{$gene}->{$var->mstr} );
							}
						}
						#if( scalar(%{$nonrefAAV{$gene}}) == 0){
						#	delete($nonrefAAV{$gene});	# delete the whole subhash for the gene with no unique variants
						#}
					}
					$mr_cl_nonrefAAV{$mr}->{$cluster_a} = \%nonrefAAV;			
				}				
			}
		  }
		}

		# WRITE EXCEL
		write_cluster_summary_excel(	\%opt,
						$summary_file_excel,
						\%mr_period_cl_data,
						\@data2,
						\%mr_clusterset,
						\%mr_clust_medGR,
						\%mr_clust_maxGR,
						\%mr_cl_AAV,
						\%mr_cl_NTV,
						\%mr_cl_uniqueAAV,
						\%mr_cl_nonrefAAV,
						\%refAAV);
	}
}

# WRITE CLUSTER SUMMARY TO EXCEL
#
# NOTE: %mr_cl_AAV, %mr_cl_uniqueAAV, %mr_cl_nonrefAAV and %refAAV are OPTIONAL
# 
sub write_cluster_summary_excel{
	my %opt					= %{shift()};
	my $summary_file_excel	= shift();	
	my %mr_period_cl_data	= %{shift()};
	my @data2				= @{shift()};
	my %mr_clusterset		= %{shift()};
	my %mr_clust_medGR		= %{shift()};
	my %mr_clust_maxGR		= %{shift()};
	my %mr_cl_AAV			= (scalar(@_)>0)? %{shift()} : ();
	my %mr_cl_NTV			= (scalar(@_)>0)? %{shift()} : ();
	my %mr_cl_uniqueAAV		= (scalar(@_)>0)? %{shift()} : ();
	my %mr_cl_nonrefAAV		= (scalar(@_)>0)? %{shift()} : ();
	my %refAAV				= (scalar(@_)>0)? %{shift()} : ();
	my $refAAVstr			= (scalar(%refAAV)>0)? AAVariantHash2AAVariantString(\%refAAV) : 'NA';
	
	# WRITE TO EXCEL
	my $workbook = Excel::Writer::XLSX->new( $summary_file_excel );
	
		# Sheet clusterSeqN: sequence counts for each cluster
	my $worksheet = $workbook->add_worksheet('clustSeqN');
	my $row = 0;
	$worksheet->write($row,0,'period');
	$worksheet->write($row,1,'mr');
	$worksheet->write($row,2,'cluster');
	$worksheet->write($row,3,'seqn');
	$worksheet->write($row,4,'subclustern');
	$worksheet->write($row,5,'support');
	$row++;
	foreach my $mr(sort {$b <=> $a} keys %mr_period_cl_data){
		my @clusters = sort {$a <=> $b} keys %{$mr_clusterset{$mr}};
		foreach my $cl(@clusters){
			foreach my $period(sort keys %{$mr_period_cl_data{$mr}}){
				
				if( defined($mr_period_cl_data{$mr}->{$period}->{$cl} ) ){
					$worksheet->write($row,0, $period);
					$worksheet->write($row,1, $mr);
					$worksheet->write($row,2, $cl);
					$worksheet->write($row,3, $mr_period_cl_data{$mr}->{$period}->{$cl}->[0]);
					$worksheet->write($row,4, $mr_period_cl_data{$mr}->{$period}->{$cl}->[1]);
					$worksheet->write($row,5, $mr_period_cl_data{$mr}->{$period}->{$cl}->[2]);
					$row++;
				}
	}}}

		# Sheet clusterSeqID: sequence ids for each cluster
	$worksheet = $workbook->add_worksheet('clustSeqID');
	$row 	= 0;
	$worksheet->write($row,0,'period');
	$worksheet->write($row,1,'mr');
	$worksheet->write($row,2,'cluster');
	$worksheet->write($row,3,'seqid');
	foreach my $recordp(@data2){
		$row++;
		my @records = @{$recordp};
		for(my $col = 0; $col<scalar(@records); $col++){
			$worksheet->write($row,$col,$records[$col]);
		}
	}
	
	# Sheets MR=10, MR=20.. : Cluster sequence counts across periods/timeperiods + GR + SupportValues
	foreach my $mr( sort {$b <=> $a} keys %mr_period_cl_data ){
		my $label	= sprintf("clustGR_MR=%f",$mr);
		my $worksheet = $workbook->add_worksheet($label);
		
		# write headers
		my $row = 0;
		my $col = 0;
		$worksheet->write($row,$col,'cluster');
		$col++;
		foreach my $period(sort keys %{$mr_period_cl_data{$mr}}){
			$worksheet->write($row,$col,$period);
			$col++;
		}
		$worksheet->write($row,$col,'medGR');
		$col++;
		$worksheet->write($row,$col,'maxGR');
		$col++;
		$worksheet->write($row,$col,'support');	
		$row++;
		
		my %clust_medGR = %{$mr_clust_medGR{$mr}};
		my %clust_maxGR = %{$mr_clust_maxGR{$mr}};
		my @clusters = sort {$clust_maxGR{$b} <=> $clust_maxGR{$a}} keys %{$mr_clusterset{$mr}};
		foreach my $cl(@clusters){
			my $cl_str = "Clust $cl";
			if($cl == -1){
				#$cl_str = "Singletons";
				next;
			}
			$col = 0;
			$worksheet->write($row,$col,$cl_str);
			$col++;
			my @periods	= sort keys %{$mr_period_cl_data{$mr}};
			foreach my $period(@periods){
				if(defined($mr_period_cl_data{$mr}->{$period}->{$cl})){
					my $seqn = $mr_period_cl_data{$mr}->{$period}->{$cl}->[0];
					if($mr_period_cl_data{$mr}->{$period}->{$cl}->[1] > 0){	# Marking nonmonophyletic clusters with *
						$seqn = "$seqn*";
					}
					$worksheet->write($row,$col, $seqn);
				}
				else{
					$worksheet->write($row,$col, 0);
				}
				$col++;
			}
			$worksheet->write($row,$col,$clust_medGR{$cl});
			$col++;
			$worksheet->write($row,$col,$clust_maxGR{$cl});
			$col++;
			$worksheet->write($row,$col, $mr_period_cl_data{$mr}->{$periods[0]}->{$cl}->[2]);
			$row++;
		}
		$col = 0;
		$worksheet->write($row,$col,"Singletons");
		foreach my $period(sort keys %{$mr_period_cl_data{$mr}}){
			$col++;
			$worksheet->write($row,$col, $mr_period_cl_data{$mr}->{$period}->{'-1'}->[0]);
		}
			$worksheet->write($row,$col+1,0);
			$worksheet->write($row,$col+2,0);
		$row++;	
		$col = 0;
		$worksheet->write($row,$col,"Total");
		foreach my $period(sort keys %{$mr_period_cl_data{$mr}}){
			$col++;
			$worksheet->write($row,$col, $mr_period_cl_data{$mr}->{$period}->{'total'}->[0]);
		}
			$worksheet->write($row,$col+1,$clust_medGR{'total'});
			$worksheet->write($row,$col+2,$clust_maxGR{'total'});
	}

	# Sheets clustMutations: CLUSTER VARIANTS
	if( scalar(%mr_cl_AAV)>0){
	foreach my $mr( sort {$b <=> $a} keys %mr_cl_AAV ){
		my $label	= sprintf("clustMutations_MR=%f",$mr);
		my $worksheet = $workbook->add_worksheet($label);
		
		# write headers
		my $row = 0;
		my $col = 0;
		$worksheet->write($row,$col,'Cluster');
		$col++;
		$worksheet->write($row,$col,'NT');
		$col++;
		$worksheet->write($row,$col,'AA');
		$col++;
		$worksheet->write($row,$col,'uniqueAA');
		$col++;	
		if($opt{'refvar'}){
			$worksheet->write($row,$col,'refAA');
			$col++;
			$worksheet->write($row,$col,'nonrefAA');
			$col++;
			$worksheet->write($row,$col,'nonrefSpikeAA');
			$col++;
			$worksheet->write($row,$col,'#nonrefAA');
			$col++;
			$worksheet->write($row,$col,'#nonrefSpikeAA');
			$col++;				
			$row++;
		}
		my %clust_maxGR 	= %{$mr_clust_maxGR{$mr}};
		my @clusters 		= sort {$clust_maxGR{$b} <=> $clust_maxGR{$a}} keys %{$mr_clusterset{$mr}};
		if(defined($mr_cl_nonrefAAV{$mr})){
			my %clust_nonrefAAV 	= %{$mr_cl_nonrefAAV{$mr}};
			my %clust_nonrefn	= ();
			foreach my $clust(@clusters){
				my $count = 0;
				if( defined($clust_nonrefAAV{$clust}) ){
				  foreach my $gene(keys %{$clust_nonrefAAV{$clust}} ){
					$count += scalar( %{$clust_nonrefAAV{$clust}->{$gene}} );
				  }
				}
				$clust_nonrefn{$clust} = $count;
			}
			@clusters = sort{$clust_nonrefn{$b} <=> $clust_nonrefn{$a}} @clusters;
		}
				
		foreach my $cl(@clusters){
			my $cl_str = "Clust $cl";
			if($cl == -1){next;}	# skip singletons
			#if( !defined($mr_cl_AAV{$mr}->{$cl}) ){ next;}
			$col = 0;
			$worksheet->write($row,$col,$cl_str);
			$col++;
			if( defined($mr_cl_AAV{$mr}) && defined($mr_cl_AAV{$mr}->{$cl}) ){
				# NT
				$worksheet->write($row,$col, AAVariantHash2AAVariantString($mr_cl_NTV{$mr}->{$cl}) ); 
				$col++;
				# AA
				$worksheet->write($row,$col, AAVariantHash2AAVariantString($mr_cl_AAV{$mr}->{$cl}) );
				$col++;
				# uniqueAA
				if( defined($mr_cl_uniqueAAV{$mr}->{$cl})){
					$worksheet->write($row,$col, AAVariantHash2AAVariantString($mr_cl_uniqueAAV{$mr}->{$cl})  ); 	
					$col++;
				}				
				if($opt{'refvar'}){
					# refAA
					$worksheet->write($row,$col, $refAAVstr);			
					$col++; 
					
					if( defined($mr_cl_nonrefAAV{$mr}->{$cl}) ){
						# nonrefAA
						my %nonrefAAV = %{ $mr_cl_nonrefAAV{$mr}->{$cl} };
						$worksheet->write($row,$col,AAVariantHash2AAVariantString(\%nonrefAAV) ); 
						$col++;
						
						# nonrefSpikeAA
						my @nonrefSpikeAAV = ();
						foreach my $var(sort {$a->ntpos <=> $b->ntpos} values %{ $nonrefAAV{'S'}} ){
							push(@nonrefSpikeAAV,$var->mstr);
						}
						$worksheet->write($row,$col, join(',',@nonrefSpikeAAV));	
						$col++;
						
						# nonrefAA count
						my @nonrefAAV = ();
						foreach my $gene(keys %nonrefAAV){
							push(@nonrefAAV,values %{$nonrefAAV{$gene}});
						}
						$worksheet->write($row,$col, scalar(@nonrefAAV) );
						$col++;
						
						# nonrefSpikeAA count
						$worksheet->write($row,$col, scalar(@nonrefSpikeAAV));
						$col++;
					}		
				}
			}
			$row++;
		}
	}
	
		# Sheets clustMutationTable: CLUSTER MUTATION TABLE
	foreach my $mr( sort {$b <=> $a} keys %mr_cl_AAV ){
		my $label	= sprintf("clustMutationTable_MR=%f",$mr);
		my $worksheet = $workbook->add_worksheet($label);
		
		my %clust_maxGR = %{$mr_clust_maxGR{$mr}};
		my @clusters = sort {$clust_maxGR{$b} <=> $clust_maxGR{$a}} keys %{$mr_clusterset{$mr}};		

		# collect lineage and cluster aa variants here
		my %allAAV = (); 
		if( scalar(%refAAV) > 0){
			foreach my $gene(keys %refAAV){
				my %tmp = ();
				$allAAV{$gene} = \%tmp;
				foreach my $var(  @{$refAAV{$gene}} ){
					$allAAV{$gene}->{$var->mstr} = $var;
				}
			}
		}
		
		my $cl_count = 0;
		foreach my $cl(@clusters){
			$cl_count++;
			if( $opt{'mutation_table_climit'} && $cl_count > $opt{'mutation_table_climit'}){
				last;
			}
			if( !defined($mr_cl_AAV{$mr}->{$cl})){ next; }
			
			my %AAV = %{$mr_cl_AAV{$mr}->{$cl}};
			
			foreach my $gene(keys %AAV){
				if( !defined($allAAV{$gene})){
					my %tmp = ();
					$allAAV{$gene} = \%tmp;
				}
				foreach my $k(keys %{$AAV{$gene}}){
					#print "cl=$cl gene=$gene k=$k\n";
					$allAAV{$gene}->{$k}= $AAV{$gene}->{$k};
				}
			}
		}
		
		# row 0: Genenames
		my @genes = keys %allAAV;
		@genes  = order_genes(\@genes);
		my $row = 0;
		my $col = 1;
		my $col_end   = 0;
		
		my $format_str = $workbook->add_format(align => 'center', left => 1, bold => 1);
		foreach my $gene( @genes ){
			my $mutation_count = scalar(keys %{$allAAV{$gene}});
			#print "gene=$gene mut_count=$mutation_count\n";
			if($mutation_count > 1){
				$worksheet->merge_range($row,$col,$row,($col+$mutation_count-1),$gene,$format_str);
			}
			else{
				$worksheet->write($row,$col,$gene,$format_str);
			}
			$col += $mutation_count;
			$col_end+= $mutation_count;
		}
		
		
		# row 1: Mutations (= variants)
		$row++;
		$col = 1;
		my $format_vertical 		= $workbook->add_format(align => 'center',valign => 'bottom', rotation => 90);
		my $format_vertical_bold	= $workbook->add_format(align => 'center',valign => 'bottom', rotation => 90, bold=> 1);
		my $format_vertical_withborder 	= $workbook->add_format(align => 'center',valign => 'bottom', rotation => 90, left => 1);
		my $format_vertical_withborder_bold= $workbook->add_format(align => 'center',valign => 'bottom', rotation => 90, left => 1, bold => 1);
		$worksheet->set_column(1, $col_end, 2); # column width for the matrix part of the mutation table
		
		foreach my $gene( @genes ){
			my @geneAAV = sort{ ($a->aapos <=> $b->aapos) || ($a->mstr cmp $b->mstr) }  values %{$allAAV{$gene}};
			my $first = 1;
			
			my %gene_refAAV = map { ($_->mstr) => $_ } ( @{$refAAV{$gene}} );
			
			foreach my $var( @geneAAV ){	
				if($first){
					if( defined( $gene_refAAV{$var->mstr} ) ){
						$worksheet->write_string($row,$col, $var->mstr, $format_vertical_withborder);
					}
					else{
						$worksheet->write_string($row,$col, $var->mstr, $format_vertical_withborder_bold);
					}
					$first = !1;
				}
				else{
					if( defined( $gene_refAAV{$var->mstr} ) ){
						$worksheet->write_string($row,$col, $var->mstr, $format_vertical);
					}
					else{
						$worksheet->write_string($row,$col, $var->mstr, $format_vertical_bold);
					}
				}
				$col++;
			}
		}		
		
		# row 2: Mutation matrix for reference + clusters (1/0)
		$row++;
		if( scalar(%refAAV)> 0){
			$col = 0;
			$worksheet->write_string($row,$col,"refvar");
			$col++;
			foreach my $gene( @genes ){
				my @geneAAV = sort{ ($a->aapos <=> $b->aapos) || ($a->mstr cmp $b->mstr) }  values %{$allAAV{$gene}};
				my %gene_refAAV = map { ($_->mstr) => $_ } ( @{$refAAV{$gene}} );
				foreach my $var( @geneAAV ){		
					my $hit = defined( $gene_refAAV{$var->mstr} ) ? 1 : 0;
					$worksheet->write_number($row,$col,$hit);
					$col++;
				}
			}
			$row++;
		}

		$cl_count = 0;
		foreach my $cl(@clusters){
			if($cl == -1){next;}
			if( !defined($mr_cl_AAV{$mr}->{$cl})){ next; }
			$cl_count++;
			if( $opt{'mutation_table_climit'} && $cl_count > $opt{'mutation_table_climit'}){
				last;
			}
			$col = 0;
			$worksheet->write_string($row,$col,"Clust $cl");
			$col++;
			my %clAAV = %{$mr_cl_AAV{$mr}->{$cl}};
		 	foreach my $gene( @genes ){
				my @geneAAV = sort{ ($a->aapos <=> $b->aapos) || ($a->mstr cmp $b->mstr) }  values %{$allAAV{$gene}};
		
				foreach my $var( @geneAAV ){		
					my $hit = defined( $clAAV{$gene}->{$var->mstr}) ? 1 : 0;
					$worksheet->write_number($row,$col,$hit);
					$col++;
				}
			}
			$row++;
		}
	}
	
	}# END Sheets clustMutations
	print STDERR "\tCluster summary printed to $summary_file_excel\n";	
}


sub write_lineage_summary{

	my %opt			= %{shift(@_)};
	my $excel_file	= "$opt{'res'}/lineageSummary.xlsx";

	if($opt{'v'}){
		print STDERR "\twriting lineage VCF(S) summary to $excel_file\n";
	}

	# READ LINEAGE VCALL_TABLE
	my %lineage_AAVstr	= ();
	if( $opt{'refvar'} ){
		read_tohash($opt{'refvar'},0,1,\%lineage_AAVstr);
	}

	# WRITE TO EXCEL
	my $workbook = Excel::Writer::XLSX->new( $excel_file );

	# LINEAGE MUTATIONS SHEET
	my $worksheet = $workbook->add_worksheet('lineageMutations');
	my $row = 0;
	$worksheet->write($row,0,'lineage');
	$worksheet->write($row,1,'NT');
	$worksheet->write($row,2,'AA');
	if($opt{'refvar'}){
		$worksheet->write($row,3,'refAA');
		$worksheet->write($row,4,'nonrefAA');
		$worksheet->write($row,5,'nonrefSpikeAA');
		$worksheet->write($row,6,'nonrefAA_minfreq10%');
	}
	$row++;		

	my @lineage_vcf_list = <$opt{'res'}/*.vcf>;
	my @lineageid_list = ();
	foreach my $lineage_vcf(@lineage_vcf_list){

		$lineage_vcf 	=~ m/\/([^\/]+)\.vcf$/;
		my $lineageid	= $1;
		push(@lineageid_list,$lineageid);

		# Lineage AA Variants
		my %vdata 	= parseVariants($lineage_vcf, 
						$opt{'vcall_freq_th'},
						$opt{'vcall_count_th'},
						$opt{'vcall_fromnt'},
						$opt{'vcall_tont'},
						$opt{'vcall_nt_allele_maxlen'});
		my %AAV		= %{$vdata{'bygene2'} };
		my %NTV		= %{$vdata{'nt.bygene'} };
		
		# Lineage ref AA variants (e.g. GISAID variants for each lineage)
			# %hash->@array form
		my %refAAV	= defined($lineage_AAVstr{$lineageid}) ? AAVariantString2AAVariantHash($lineage_AAVstr{$lineageid}) : ();
			# convert to %hash->%hash form
		my %refAAV2	= ();
		foreach my $gene(%refAAV){
			my %tmp = ();
			$refAAV2{$gene} = \%tmp;
			foreach my $var( @{$refAAV{$gene}} ){
				$refAAV2{$gene}->{$var->mstr} = $var;
			}
		}

		# nonref AA Variants
		my %nonrefAAV 	= %{clone(\%AAV)};
		if( scalar(%refAAV)>0 ){
			# For each gene delete all refvars
			foreach my $gene(keys %refAAV){
				if(defined($nonrefAAV{$gene})){
					foreach my $var( @{$refAAV{$gene}} ){
						delete( $nonrefAAV{$gene}->{$var->mstr} );
					}
				}
			}
		}

		# PRINT EXCEL SHEET
		$worksheet->write($row, 0, $lineageid);
		$worksheet->write($row, 1, AAVariantHash2AAVariantString(\%NTV) );
		$worksheet->write($row, 2, AAVariantHash2AAVariantString(\%AAV) );
		if($opt{'refvar'}){
		  $worksheet->write($row, 3, AAVariantHash2AAVariantString(\%refAAV) );
		  $worksheet->write($row, 4, AAVariantHash2AAVariantString(\%nonrefAAV) );
		  # Spike nonref AAV
		  my @nonrefSpikeAAV = ();
		  foreach my $var(sort {$a->ntpos <=> $b->ntpos} values %{$nonrefAAV{'S'} }){
			push(@nonrefSpikeAAV,$var->mstr);
		  }
		  $worksheet->write($row, 5, join(',',@nonrefSpikeAAV));
		  # nonrefAA_minfreq10%
		  my @nonref_fmin10 = ();
		  my @genes = keys %nonrefAAV; @genes = order_genes(\@genes);
		  foreach my $gene( @genes ){
		  	my @vars = ();
		  	foreach my $var(sort {$a->ntpos <=> $b->ntpos} values %{$nonrefAAV{$gene} }){
				if($var->freq >= 0.10){
					push(@vars,$var->mstr);
				}
			}
			if(scalar(@vars)>0){
				push(@nonref_fmin10, join(': ',$gene,join(',',@vars)));
			}
		  }
		  $worksheet->write($row, 6, join('; ',@nonref_fmin10));
		}
		$row++;
		
		
		# SEPARATE MUTATION SHEET FOR EACH LINEAGE
		my $worksheet2 = $workbook->add_worksheet("$lineageid");
		my $row = 0;
		$worksheet2->write($row,0,'ntpos');
		$worksheet2->write($row,1,'aapos');
		$worksheet2->write($row,2,'gene');
		$worksheet2->write($row,3,'HGVSp');
		$worksheet2->write($row,4,'HGVSc');
		$worksheet2->write($row,5,'mstr');
		$worksheet2->write($row,6,'count');
		$worksheet2->write($row,7,'freq');
		if($opt{'refvar'}){
			$worksheet2->write($row,8,'refvar')
		}
		$row++;
		my @genes = keys %AAV; @genes = order_genes(\@genes);
		foreach my $gene(@genes){
			my @vars = ();
		  	foreach my $var(sort {$a->ntpos <=> $b->ntpos} values %{ $AAV{$gene} }){
				$worksheet2->write($row,0,$var->ntpos);
				$worksheet2->write($row,1,$var->aapos);
				$worksheet2->write($row,2,$var->gene);
				$worksheet2->write($row,3,$var->HGVSp);
				$worksheet2->write($row,4,$var->HGVSc);
				$worksheet2->write($row,5,$var->mstr);
				$worksheet2->write($row,6,$var->count);
				$worksheet2->write($row,7,$var->freq);
				if($opt{'refvar'}){
				  my $refvar = (defined($refAAV2{$gene}->{$var->mstr})) ? 'YES': 'NO';
				  $worksheet2->write($row,8,$refvar)
				}
				$row++;
			}
		}
		
	}			
}


# Writes clusetr MSAs for each $lineage x $mr x $cluster combination
# The first two sequneces in output cluster MSA(s) will be reference genome + outgroup (if not identical to reference genome)
#
# USAGE: write_cluster_msa(\%opt)
# 
sub write_cluster_msas{

	my %opt		= %{shift(@_)};
	
	
	# Iterate lineage msa
	my @msa_list = <$opt{'res'}/*.msa>;
	
	foreach my $msa(@msa_list){		
		$msa	 					=~ m/\/([^\/]+)\.msa$/i;
		my $lineageid				= $1;
		my $msa_withrefseq			= "$msa.withrefseq";
		
		
		if($opt{'target'}){	# LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){ next;}
		}
				
		# remove old faidx if present
		system_call( "rm -f $opt{'res'}/$lineageid.fai", $opt{'v'} );
	
		
		# READ CLUSTER FILES + CREATE MSA(S)
		foreach my $mr(@clustering_mutation_rates){
			if($opt{'v'}){ print STDERR "\t\tWriting msa(s) for: $lineageid mr=$mr\n";}
			my $clust_file		= "$opt{'res'}/$lineageid.mr=$mr.cl";
			my %seqid_clust 	= ();
			read_tohash($clust_file,0,1,\%seqid_clust);
			my %clust_seqids	= invert_hash(\%seqid_clust);
			delete($clust_seqids{'-1'});

			foreach my $cluster(sort {$a <=> $b} keys %clust_seqids){
				my $outdir			= "$opt{'res'}/$lineageid/mr=$mr";
				system("mkdir -p $outdir");
				my $msa_flt			= "$opt{'wrkdir'}/msaflt.tmp";
				my $msa_out 		= "$outdir/$lineageid.mr=$mr.cl=$cluster.msa";						
				my @seqids 			= sort @{$clust_seqids{$cluster}};
				if(scalar(@seqids) < $opt{'minseqn'}){ next; }
				
				# add outgroup + refseq ids to each cluster
				my %seqids_h 					= ();
				$seqids_h{$opt{'refgen_id'}}	= 1;	# we print refgen at top
				$seqids_h{$opt{'outgroup_id'}} 	= 2;	# then outgroup
				foreach(@seqids){						# then the rest of cluster seqs
					$seqids_h{$_} 				= 3;
				}
				@seqids						= sort {$seqids_h{$a} <=> $seqids_h{$b}} keys %seqids_h;
				
				open(OUT,">$msa_flt") or die "Can\'t open $msa_flt: $!\n";
				foreach my $seqid(@seqids){
					print OUT "$seqid\n";}
				close(OUT);

				# SELECT CLUSTER SEQS + OUTGROUP WITH SEQKIT
				system_call( "seqkit faidx --ignore-case -w0 $msa_withrefseq --infile-list $msa_flt 1> $msa_out", $opt{'v'} );
								
				# TRIMM GAPS IN REFSEQ:			
				system_call( "seqkit grep -i -w0 -p $opt{'refgen_id'} $msa_out 1> $opt{'wrkdir'}/msa_refseq.faa", $opt{'v'} );
				system_call( "seqkit locate -rP --bed -p \"-+\"  $opt{'wrkdir'}/msa_refseq.faa | tail -n+2  1> $opt{'wrkdir'}/msa_gaps.bed", $opt{'v'} );
				my %gaps_hash= ();
				read_tohash("$opt{'wrkdir'}/msa_gaps.bed",1,2,\%gaps_hash);
				foreach my $start(keys %gaps_hash){ $gaps_hash{$start}--; } # set end pos -1
				my @gaps_list = ();
				foreach my $start(sort {$a <=> $b} keys %gaps_hash){
					push(@gaps_list,"$start".'-'.$gaps_hash{$start});
				}
				if(scalar(@gaps_list)>0){
					my $gaps_str = join(",",@gaps_list);
					system_call( "trimal -in $msa_out -select { $gaps_str } | seqkit seq -w0 -i -u 1> $msa_out.gapflt", $opt{'v'} );
					system_call( "mv $msa_out.gapflt $msa_out", $opt{'v'} );
				}
				system_call("rm -f $msa_out.gapflt", $opt{'v'} );
			}
		}
		print STDERR "\tCluster MSA(s) printed to $opt{'res'}/$lineageid\n";
	}
}

# For each cluster msa create VCF file with both nt and aa variants
sub write_cluster_vcfs{

	my %opt			= %{shift(@_)};
	my @msa_list 	= <$opt{'res'}/*.msa>;
	my $log 		= "$opt{'log'}/vcall_cluster.log";
	system("echo \"\" >  $log");
	
	foreach my $msa(@msa_list){		
		$msa			=~ m/\/([^\/]+)\.msa$/i;
		my $lineageid	= $1;
		my $lineage_vcf	= "$opt{'res'}/$lineageid.vcf";

		if($opt{'target'}){	# LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){ next;}
		}
		
		# READ MSA(S) AND CREATE VCF FILES
		foreach my $mr(@clustering_mutation_rates){
			if($opt{'v'}){
				print STDERR "\t\tWriting vcf for: $lineageid mr=$mr\n";
			}
			
			my $clust_file		= "$opt{'res'}/$lineageid.mr=$mr.cl";
			my %seqid_clust 	= ();
			read_tohash($clust_file,0,1,\%seqid_clust);
			my %clust_seqids	= invert_hash(\%seqid_clust);
			delete($clust_seqids{'-1'});	
			
			foreach my $cluster(sort {$a <=> $b} keys %clust_seqids){
				my $outdir		= "$opt{'res'}/$lineageid/mr=$mr";
				my $msa_file 	= "$outdir/$lineageid.mr=$mr.cl=$cluster.msa";
				my $vcf_file	= "$outdir/$lineageid.mr=$mr.cl=$cluster.vcf";				
				my @seqids 		= sort @{$clust_seqids{$cluster}};
				if(scalar(@seqids) < $opt{'minseqn'}){ next; }
				
				if( !(-e $msa_file) ){
					print STDERR "WARNING: no msa $msa_file: skipping\n";
					next;
				}
				
				# CALL NT VARIANTS
				system_call("$opt{'vcall_nt_call'} --REF $opt{'refgen_id'} -c \"$opt{'refgen_id'}\" $msa_file 1> $vcf_file.tmp 2>> $log", $opt{'v'});
				
				# LOCATE GAPPY SITES TO EXCLUDE FROM REPORTED VARIANTS
				my $nonempty = print_gaps2bed($msa_file, "$opt{'wrkdir'}/tmp.bed", 
									$opt{'refgen_id'}, 
									$opt{'vcall_gapmask_coverage_th'}, 
									$opt{'vcall_gapmask_gaplength_th'});
				system("echo -e \"$opt{'refgen_id'}\\t-1\\t$opt{'vcall_fromnt'}\" >> $opt{'wrkdir'}/tmp.bed");
				system("echo -e \"$opt{'refgen_id'}\\t$opt{'vcall_tont'}\\t$opt{'refgen_len'}\" >> $opt{'wrkdir'}/tmp.bed");
				
				filterVCF(  "$vcf_file.tmp", $vcf_file, "$opt{'wrkdir'}/tmp.bed");
				system_call( "rm -f $vcf_file.tmp", $opt{'v'} );
				
				# CALL FUNCTIONAL VARIANTS
				if($opt{'vcall_aa'}){
				 	system_call( "$opt{'vcall_aa_call'} $opt{'refgen_id'} $vcf_file 1> $opt{'wrkdir'}/tmp.vcf", $opt{'v'} );
					system_call( "mv -f $opt{'wrkdir'}/tmp.vcf $vcf_file", $opt{'v'} );
				}
			}
		}
		print STDERR "\tCluster VCF files printed to $opt{'res'}/$lineageid\n";
	}
}	

sub write_lineage_vcfs{

	my %opt		= %{shift(@_)};
	my $log 		= "$opt{'log'}/vcall_lineage.log";
	system("echo \"\" >  $log");
	
	if($opt{'v'}){
		print STDERR "\tgenerating lineage VCF(S)\n";
	}

	# Iterate lineage(s)
	my @lineage_msa_list = <$opt{'res'}/*.msa.withrefseq>;
	
	foreach my $msa(@lineage_msa_list){
			
		$msa		 		=~ m/\/([^\/]+)\.msa\.withrefseq$/;
		my $lineageid		= $1;
		my $vcf				= "$opt{'res'}/$lineageid.vcf";

		if($opt{'target'}){	# LIMIT ANALYSIS TO TARGET LINEAGES
			if( !defined($opt{'target'}->{$lineageid}) ){ next;}
		}		
		
		# REMOVE cols in $lineageid.msa.withrefseq that contain gaps (if any) in reference sequence
		system_call("seqkit grep -i -w0 -p $opt{'refgen_id'} $msa 1> $opt{'wrkdir'}/msa_refseq.faa", $opt{'v'});
		system_call("seqkit locate -rP --bed -p \"-+\"  $opt{'wrkdir'}/msa_refseq.faa | tail -n+2  1> $opt{'wrkdir'}/msa_gaps.bed", $opt{'v'});
		my %gaps_hash= (); read_tohash("$opt{'wrkdir'}/msa_gaps.bed",1,2,\%gaps_hash);
		foreach my $start(keys %gaps_hash){ $gaps_hash{$start}--; } # set end pos -1
		my @gaps_list = ();
		foreach my $start(sort {$a <=> $b} keys %gaps_hash){
			push(@gaps_list,"$start".'-'.$gaps_hash{$start});
		}
		my $gaps_str = join(",",@gaps_list);
		if($gaps_str ne ""){
			system_call( "trimal -in $msa -select { $gaps_str } | seqkit seq -w0 -i -u 1> $opt{'wrkdir'}/msa.gapflt", $opt{'v'} );
			system_call( "mv $opt{'wrkdir'}/msa.gapflt $msa", $opt{'v'} );
		}
		
		# LOCATE GAPPY SITES TO EXCLUDE FROM VARIANT CALLING
		my $nonempty = print_gaps2bed($msa, "$opt{'wrkdir'}/msa_gaps.bed", $opt{'refgen_id'}, 
										$opt{'vcall_gapmask_coverage_th'}, $opt{'vcall_gapmask_gaplength_th'});
										
		system("echo -e \"$opt{'refgen_id'}\\t-1\\t$opt{'vcall_fromnt'}\" >> $opt{'wrkdir'}/msa_gaps.bed");
		system("echo -e \"$opt{'refgen_id'}\\t$opt{'vcall_tont'}\\t$opt{'refgen_len'}\" >> $opt{'wrkdir'}/msa_gaps.bed");				
		
		# CALL NT VARIANTS
		system_call( "$opt{'vcall_nt_call'} --REF $opt{'refgen_id'} -c \"$opt{'refgen_id'}\" $msa 1> $vcf.tmp 2>> $log");
		filterVCF( "$vcf.tmp", $vcf, "$opt{'wrkdir'}/msa_gaps.bed" );
				
		# CALL FUNCTIONAL VARIANTS
		if($opt{'vcall_aa'}){
			system_call( "$opt{'vcall_aa_call'} $opt{'refgen_id'} $vcf 1> $opt{'wrkdir'}/tmp.vcf", $opt{'v'} );
			system_call( "mv $opt{'wrkdir'}/tmp.vcf $vcf", $opt{'v'} );
		}
	}
}


# CREATE A MULTI-TREE NEXUS FILE
# Usage: write_multi_trees(\%opt, \@res_dirs)
#
sub write_multi_trees{
	my %opt		= %{shift(@_)};
	my @res_dirs	= @{shift(@_)};
	
	if(scalar(@res_dirs)>1){
		print STDERR "\n\t\e[4m Create timeseries nexus trees \e[0m\n";
		
		my @file_list	 	= <$res_dirs[0]/*.con.tree>;
		my @lineage_list	= ();
		foreach my $file(@file_list){
			my $prefix	= $file;
			$prefix 	=~ s/\.(con\.tree)$//;
			$prefix		=~ m/\/([^\/]+)$/;
			my $lineage	= $1;
			push(@lineage_list,$lineage);
		}	
	
		foreach my $lineage(@lineage_list){
		    MR_LOOP: foreach my $mr(@clustering_mutation_rates){
		    	# check that files exist and collect
			my @trees 	= ();
			my @clust_files	= ();
			foreach my $res_dir(@res_dirs){
				if( !(-e "$res_dir/$lineage.con.tree") ){ print STDERR "\tno file:$res_dir/$lineage.con.tree: skipping"; next MR_LOOP; }
				if( !(-e "$res_dir/$lineage.mr=$mr.cl")){  print STDERR "\tno file:$res_dir/$lineage.mr=$mr.cl: skipping"; next MR_LOOP; }
				
				push(@trees,"$res_dir/$lineage.con.tree");
				push(@clust_files, "$res_dir/$lineage.mr=$mr.cl");
			}
			# Color monophyletic clusters (clusters with no subclusters)
			my $tree_str = join(' ',@trees);
			my $clust_str = join(' ',@clust_files);
			system_call( "perl tree_colorclust.pl --tree $tree_str --clust $clust_str --colpan $opt{'colpan_str'} --monophyl 1> $res_dirs[0]/$lineage.mr=$mr.tseries.nex", $opt{'v'});
		    }
		}
	 }
}

# system_call($system_call, $verbal=false)
#
# $system_call	String with the system call to excecute
# $verbal	Set true to print the call prior to excecuting the call. Optional, default is false.
# 
sub system_call{
	my $call	= shift;
	my $verbal	= (scalar(@_)>0) ? shift(@_): 0;
	if($verbal){
		print STDERR "\t$call\n";
	}
	my @args= ("bash","-c",$call);
	system(@args) == 0 or die $!;
}


# read_tohash(file,key_ind,val_ind,\%hash)
# 
# Does not support multiple values for a single key. The last value encountered is saved.
sub read_tohash{
	my $file= shift;
	my $keyi= shift;
	my $vali= shift;
	my $hashp= shift;
	#print STDERR "# read_tohash from $file\n";
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
        	$ln++;
		if($l =~ m/^[@#!]/){ next; }
		chomp($l);
		my @sp= split(/\t/,$l,-1);
        	$hashp->{$sp[$keyi]} = $sp[$vali];
	}
	close(IN);
}

# my %val_keylist = invert_hash(\%key_val_hash)
#
# Inverts key->val hash to val->@key_list hash
sub invert_hash{
	my %hash = %{shift(@_)};
	my %inv = ();
	foreach my $k(keys %hash){
		my $v = $hash{$k};
		if(!defined($inv{$v})){
			my @tmp = ($k);
			$inv{$v} = \@tmp;
		}
		else{
			push(@{$inv{$v}},$k);
		}
	}
	return %inv;
}

# my @table = read_totable(file)
#
# Reads an array of array pointers from a tab-delimited file. Reads all fields from each line.
sub read_totable{
	my $file = shift;
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	my @table = ();
	while(my $l=<IN>){
        	$ln++;
		if($l =~ m/^[@#!]/){ next; }
		chomp($l);
		my @sp= split(/\t/,$l,-1);
        	push(@table,\@sp);
	}
	close(IN);
	return @table;
}

# print_table($file,\@table,\@headers)
#
sub print_table{
	my $file = shift;
	my $table= shift;
	my $headers= shift;
	open(OUT,">$file") or die "Can\'t open $file: $!\n";
	print OUT join("\t",@{$headers}),"\n";
	foreach(@{$table}){
		print OUT join("\t",@{$_}),"\n";
	}
	close(OUT);
}


# print_hash_tofile(file,\%hash)
#
# Print hash to file
sub print_hash_tofile{
	my $file= shift;
	my $hashp= shift;
	open(OUT,">$file") or die "Can\'t open $file: $!\n";
	foreach my $k(sort keys %{$hashp}){
		print OUT "$k\t",$hashp->{$k},"\n";
	}
	close(OUT);
}


# RETURNS NUMBER OF SEQS IN FASTA
# par: file.fasta
sub get_seqnum{
	my $fasta = shift;
	open (IN,"<$fasta") or die "Failed to open $fasta: $!\n";
	my $seqnum = 0;
	while(my $l=<IN>){
		if($l =~ m/^>/){
			$seqnum++;
		}
	}
	close(IN);
	return $seqnum;
}

# Returns the length of the first sequence (ordered by seqid) in a fasta file
sub get_seqlen{
	my $fasta 		= shift;
	my %seqhash 	= readfasta($fasta);
	foreach my $seqid(sort keys %seqhash){ 
		return length($seqhash{$seqid});
	}
	return -1;
}

# my $lastseqid = get_lastseqid($fasta_file)
#
sub get_lastseqid{
	my $fasta = shift;
	open (IN,"<$fasta") or die "Failed to open $fasta: $!\n";
	my $id;
	while(my $l=<IN>){
		if($l =~ m/^>([\S]+)/){
			$id = $1;
		}
	}
	close(IN);
	return $id;
}

# has_seqid($fasta_file,$seqid)
# returns true if fasta file contains at least one seq with matching id (ignoring case)
#
sub has_seqid{
	my $fasta = shift;
	my $seqid = shift;
	open (IN,"<$fasta") or die "Failed to open $fasta: $!\n";
	my $id;
	while(my $l=<IN>){
		if($l =~ m/^>([\S]+)/){
			$id = $1;
			if( $id =~ m/$seqid/i ){
				return 1;
	}}}
	close(IN);
	return !1;
}	

# my %clust_lca = get_cluster_lca($tree, \%nodeid_clustid)
# $tree			Bio::Tree::TreeI
# \%nodeid_clustid	nodeid -> clustid map
# %{$clust_lca}		clustid->lca hash
#
# Retrieves a list of Lowest Common Ancestors (LCAs) for each cluster in the nodeid_clustid hash.
#
sub get_cluster_lca{
	my $tree		= shift(@_);
	my %nodeid_clust	= %{shift(@_)};

	my %clust_nodes	= ();
	foreach my $nodeid(keys %nodeid_clust){
		my $clust = $nodeid_clust{$nodeid};
		if(!defined($clust_nodes{$clust})){
			my @tmp; $clust_nodes{$clust} = \@tmp;
		}
		my $node = $tree->find_node($nodeid);
		unless(defined($node)){
			print STDERR "WARNING: unknown nodeid=$nodeid\n";
			next;
		}
		push(@{$clust_nodes{$clust}},$node);
	}
	
	if(defined($clust_nodes{-1}) ){ delete($clust_nodes{-1}); }	# delete singleton "cluster"
	
	# representing clusters by LCA
	my %clust_lca 	= ();
	foreach my $clust(sort keys %clust_nodes){
		if(scalar(@{$clust_nodes{$clust}})== 1){
			$clust_lca{$clust} = $clust_nodes{$clust}->[0];
		}
		else{
			$clust_lca{$clust} = $tree->get_lca( @{$clust_nodes{$clust}} );
		}
	}	
	return %clust_lca;
}


# my %class_counts = get_classcounts_fromhash(\%class_hash)
#
# Counts the number of elements for each class defined by a hash
# \%class_hash   element_id>class_id hash
# %class_counts  a hash with counts for each class
#
sub get_classcounts{
	my %class_hash = %{shift(@_)};
	my %counts = ();
	foreach my $el(keys %class_hash){
		if(!defined($counts{$class_hash{$el}}) ){
			$counts{$class_hash{$el}} = 1;
		}
		else{
			$counts{$class_hash{$el}}++;
		}
	}
	return %counts;
}


# $node_str = get_newick_str( $node )
#
# $node		Bio::Tree::NodeI
# $node_str	Subtree rooted in $node in Newick format
#
# e.g. to get a Newick string representation of the whole tree:
# $tree_newick_str = get_newick_str( $tree->get_root_node() )
#
sub get_newick_str{
	my $node 	= shift(@_);
	my $node_str	= "";
	# Inner nodes: children
	if( !($node->is_Leaf()) ){
		$node_str .= "(";
		$node_str .= join(",", map{ get_newick_str($_) } $node->each_Descendent() );
		$node_str .= ")";
	}
	# Inner and leaf nodes: tags
	if( scalar($node->get_all_tags()) > 0 ){
		my $tag_val_str	= join(",", map { "$_=".(($node->get_tag_values($_))[0]) } $node->get_all_tags() );
		$node_str .= "[&$tag_val_str]";
	}
	# Inner and leaf nodes: id and branch length
	$node_str .= defined($node->id)? $node->id: '';
	$node_str .= defined($node->branch_length)? ":".$node->branch_length() : '';		
	return $node_str;
}

# Retuns period label for a given seqid. Assumes that seqids have date field (eg /YYYY-MM-DD/ )
# Labels can be assigned by week or by month
#
# USAGE: my $period = get_period_label($seqid,"month")
# 
# Returns: time period label OR !1 if no date-field found
# 
sub get_period_label{
	my $seqid	= shift(@_);
	my $period	= lc(shift(@_));
	my @sp		= split(/\|/,$seqid,-1);
	my $t;
	for(my $i=0; $i<scalar(@sp); $i++){
		#print "seqid \t:$seqid\n";
		#print "sp    \t:$sp[$i]\n";
		eval{
			$t = Time::Piece->strptime($sp[$i], "%F");
		}
		or do {next;}
	}
	if(defined($t)){
		#print "t\t:",$t->strftime(),"\n";
		if( $period eq 'week'){
			# my $label = ($t->year).'-wk'.($t->week);
			my $label = sprintf("%.4d-wk%.2d",($t->year),($t->week));
			return ($label);
		}
		elsif( $period eq 'month'){
			return ($t->strftime("%Y-%m"));
		}
		else{
			print STDERR "WARNING: invalid time period:$period\n";
			return !1;
		}
	}
	else{
		 print STDERR "WARNING: no valid date field in $seqid\n";
		 return !1;
	}
}


# Parses NTVariant(s) & AAVariant(s) from v4.0 VCF file
#
# Returns a hash with fields
#	"byntpos" -> an array of all AAVairant(s) in genomic order
#	"bygene" -> a hash with AAVariant(s) array for each gene: my @gene_a_vars = $bygene{$gene_a}->@*;
#	"bygene2"-> a hash with AAVariant(s) hashes for each gene, with mstr fields as keys: $bygene2{'S'}->{'A12T'} # reference to 'A12T' aa var in S-gene
#	"nt.byntpos"	-> an array of All NTVariant(s) in genomic order
#	"nt.bygene" 	-> a hash with NTVariant(s) array for each gene: my @gene1_nt_vars = $nt.bygene{$gene1}->@*
#	"nt.bygene2"	-> a hash with NTVariant(s) hashes for each gene, with mstr fields as keys: $nt.bygene2{'S'}->{'G10C'} # reference to 'G10C' nt var in S-gene
#
# Example usage:
# my %vdata = parseVariants($vcf_file.vcf, freq_th=0, count_th=0, fromnt=0, tont=-1, max_nt_len=!1, mask_file.bed)
#
# foreach my $gene(keys $vdata{'bygene'}){
#	foreach my $var($vdata{'bygene'}->{$gene}->@*){
#		fprint("ntpos:	%d\n",	$var->ntpos);
#		fprint("aapos:	%d\n",	$var->aapos);
#		fprint("gene:	%s\n",	$var->gene);
#		fprint("ref: 	%s\n",	$var->ref);
#		fprint("alt:	%s\n",	$var->alt);
#		fprint("HGVSp:	%s\n",	$var->HGVSp);
#		fprint("count: %i\n",	$var->count);
#		fprint("freq: %.3d\n",	$var->freq);
#}}
sub parseVariants{
	my $file 	= shift(@_);
	my $freq_th	= (scalar(@_)>0) ? shift(@_): 0;
	my $count_th	= (scalar(@_)>0) ? shift(@_): 0;
	my $fromnt	= (scalar(@_)>0) ? shift(@_): 0;
	my $tont	= (scalar(@_)>0) ? shift(@_): -1;
	my $max_ntlen	= (scalar(@_)>0) ? shift(@_): !1;
	my $mask_file	= (scalar(@_)>0) ? shift(@_): !1;
	
	my %header_musthave 	= ("POS" => 1,"REF" => 1, "ALT" => 1,"INFO" => 1,"FORMAT"=> 1);
	my %header_col		= ();
	my $header_count	= scalar(keys %header_musthave);
	
	
	# READ MASK BED FILE IF SPECIFIED
	my %mask	= ();
	if($mask_file){
	open(IN,"<$mask_file") or die "Can\'t open $mask_file: $!\n";
	while(my $l=<IN>){
		my ($chrom,$from,$to) = split(/\t/,$l,3);
		$from++; $to++; 	# NOTE: BED file format has 0-based indexing of positions > shift to 1-based
		for(my $pos=$from; $pos<$to; $pos++){	
			$mask{$pos} = 1;
		}
	}}
	
	
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	# SKIP METAINFO LINES
	my $l;
	while($l=<IN>){
		if($l =~ m/^#CHROM/i){
			last;
		}
	}
	
	# READ AND MAP HEADERS
	chomp($l);
	my @sp	= split(/\t/,$l,-1);
	if(scalar(@sp)>10){	# skipping seqid names
		@sp = @sp[0..10];
	}
	for(my $col=0; $col<scalar(@sp); $col++){
		if(defined($header_musthave{$sp[$col]})){
			$header_col{$sp[$col]} = $col;
	}}
	foreach my $h(keys %header_musthave){
		if( !defined($header_col{$h}) ){
			print STDERR "ERROR: parse_aavariants: $file has no header $h: exiting\n";
			exit(1);
	}}
	
	# READ VARIANT DATA
	my @aavar_list 		= ();
	my @ntvar_list		= ();
	my %genes_visited 	= ();
	my $info_field_allele		= 0;
	my $info_field_annotation 	= 1;
	my $info_field_genename		= 3;
	my $info_field_HGVSc		= 9;
	my $info_field_HGVSp		= 10;
	my $info_field_max		= 10;
	my $info_field_protposlen	= 13;
	while(my $l=<IN>){
        	chomp($l);
		my @sp= split(/\t/,$l,-1);	
		
		#$sp[$header_col{'ALT'}] =~ s/,/\|/g; # for multi-allelic variants separate alleles with '|'
        	#my $vcall = join('',$sp[$header_col{'REF'}],$sp[$header_col{'POS'}],$sp[$header_col{'ALT'}]);
		#push(@vcall_list,$vcall);
		
		my $ntpos 	= $sp[$header_col{'POS'}];
		my $ref		= $sp[$header_col{'REF'}];
		my @alt		= split(/,/,$sp[$header_col{'ALT'}],-1);
		my $format	= $sp[$header_col{'FORMAT'}];
		my $first_gt_col= $header_col{'FORMAT'}+1;
		my @gt_list	= @sp[$first_gt_col..$#sp];
		my %allele_count= ();
		foreach my $allele (($ref,@alt)){ $allele_count{$allele} = 0; };
		my %gt2allele	= ();
		$gt2allele{0}	= $ref;
		for(my $gt=1; $gt<=scalar(@alt); $gt++){
			$gt2allele{$gt} = $alt[$gt-1];
		}
		
		if($ntpos < $fromnt){ next;}		# out of bounds: head of seq
		if( ($tont>0) && $ntpos >$tont){ next;}	# out of bounds: tail of seq
		if( $mask{$ntpos} ){ next; }		# masked out by specified bed file
		
		
		# GENOTYPE COUNTS AND FREQS
		foreach my $gt(@gt_list){
			$gt = (split(/:/,$gt))[0];	# the first entry in genotype field must be the genotype independent on $format
			if( !defined( $gt2allele{$gt}) ){
				print STDERR "WARNING: parseVariants(..): unrecognised GT entry=$gt: skipping\n";
				next;
			} 
			$allele_count{$gt2allele{$gt}}++;
		}
		
		# NTVariant(s)
		my %allele_ntvar = (); # NTVariants by allele. Used to fill in NTVariant->gene field during AAVariant parsing
		foreach my $allele(@alt){
			my $count	= defined($allele_count{$allele}) ? $allele_count{$allele} : 0;
			my $freq	= $count/scalar(@gt_list);
			my $mstr	= join('',$ref,$ntpos,$allele);
			
			if($freq <= $freq_th){ next; }			# LOW FREQUENCLY
			if($count<= $count_th){ next; }			# LOW COUNT
			if( ($max_ntlen) && length($allele)>$max_ntlen){ next; } # TOO LONG ALLELE	
			
			my $ntvar = NTVariant(ntpos=>$ntpos,gene=>'NA', ref=>$ref, alt=>$allele, mstr=>$mstr, count=>$count, freq=>$freq);
			$allele_ntvar{$allele} = $ntvar;
		}
		
		# INFO:ANN field (Printed by functional variant call)
		if( $sp[$header_col{'INFO'}] eq "" ){ next; }
		
		my $info_field 	= $sp[$header_col{'INFO'}];
		@sp		= split(/;/,$info_field,-1);
		my $has_ANNfield= !1;
		foreach my $field(@sp){
			if($field =~ m/^ANN/i){
				$field 		=~ s/^ANN=//i;
				$info_field	= $field;
				$has_ANNfield 	= 1;
				last;
			}
		}
		if( !$has_ANNfield){ next; }
		my @info_list	= split(/,/,$info_field,-1);
		my %alleles_called= (); # we call the first aa variant for each allele/GT and skip the rest
		
		foreach my $info (@info_list){
			@sp 	 = split(/\|/,$info,-1);
			if(scalar(@sp) < $info_field_max){ next;}
		
			my $allele	= $sp[$info_field_allele];
			my $annotation 	= $sp[$info_field_annotation];
			my $gene	= $sp[$info_field_genename];
			my $HGVSc	= $sp[$info_field_HGVSc];
			my $HGVSp	= $sp[$info_field_HGVSp];
			my $mstr		= $HGVSp;
			$mstr		=~ s/^p.//i;
			my $aapos	= $sp[$info_field_protposlen];	# in format aapos/prot_length
			$aapos		= (split(/\//,$aapos,2))[0];
			my $count	= defined($allele_count{$allele}) ? $allele_count{$allele} : 0;
			my $freq	= $count/scalar(@gt_list);
			
			# Fill in NTVariant->gene field
			if(defined($allele_ntvar{$allele}) && ($allele_ntvar{$allele}->gene eq 'NA') ){
				$allele_ntvar{$allele}->gene = $gene;
			}
		
			# SKIP VARIANT IF
			if($annotation =~ m/synonymous/){ next;}	# SYNONYMOUS
			if($HGVSp eq ''){ next;}			# NO HGVSp field
			if( defined($alleles_called{$allele}) ){	# ALREADY CALLED VARIANT FOR THIS ALLELE
				next;
			}
			else{
				$alleles_called{$allele} = 1;
			}
			if($freq <= $freq_th){ next; }			# LOW FREQUENCLY
			if($count<= $count_th){ next; }			# LOW COUNT		
			
			## CONVERT DELETION FORMAT
			if( $mstr =~ /^[A-Z]{1}([0-9]+)_[A-Z]{1}([0-9]+)del$/i){
				$mstr = "$1_$2del"; # remove aa letters
			}
			if( $mstr =~ /^[A-Z]{1}([0-9]+)del$/i){
				$mstr = "$1del"; # remove aa letter
			}			
				
			#my $aavar = AAVariant($ntpos,$gene,$ref,$allele,$aapos,$HGVSp,$HGVSc,$mstr,$count,$freq);
			my $aavar = AAVariant(ntpos=>$ntpos,gene=>$gene, ref=>$ref, alt=>$allele, aapos=>$aapos, 
						HGVSp=>$HGVSp,HGVSc=>$HGVSc,mstr=>$mstr, count=>$count, freq=>$freq);
			push(@aavar_list,$aavar);
			$genes_visited{$gene} = 1;
		}
		# Now NTVariant genes set according to AA variants > collect
		push(@ntvar_list, values(%allele_ntvar));
	}
	close(IN);
	
	# NTVARIANTS
	my @ntvar_byntpos = sort {$a->ntpos <=> $b->ntpos} @ntvar_list;
	my %ntvar_bygene  = ();
	my %ntvar_bygene2 = ();
	
	foreach my $var(@ntvar_byntpos){
		if(!defined($ntvar_bygene{$var->gene})){
			my @tmp = ();
			$ntvar_bygene{$var->gene}= \@tmp;
			my %tmp2= ();
			$ntvar_bygene2{$var->gene}= \%tmp2;
		}
		push( @{$ntvar_bygene{$var->gene}}, $var);
		$ntvar_bygene2{$var->gene}->{$var->mstr} = $var;
	}
	# sort bygene
	foreach my $gene(keys %ntvar_bygene){
		 my @tmp = sort {$a->ntpos <=> $b->ntpos} @{$ntvar_bygene{$gene}}; 
		 $ntvar_bygene{$gene} = \@tmp;
	}
		
	# AAVARIANTS
	my @aavar_byntpos = sort {$a->ntpos <=> $b->ntpos} @aavar_list;
	my %aavar_bygene  = ();
	my %aavar_bygene2 = ();

	foreach my $var(@aavar_byntpos){
		if(!defined($aavar_bygene{$var->gene})){
			my @tmp = ();
			$aavar_bygene{$var->gene}= \@tmp;
			my %tmp2= ();
			$aavar_bygene2{$var->gene}= \%tmp2;
		}
		push( @{$aavar_bygene{$var->gene}}, $var);
		$aavar_bygene2{$var->gene}->{$var->mstr} = $var;
	}
	# sort bygene
	foreach my $gene(keys %aavar_bygene){
		 my @tmp = sort {$a->aapos <=> $b->aapos} @{$aavar_bygene{$gene}};
		 $aavar_bygene{$gene} = \@tmp;
	}
	
	my %vdata = (byntpos => \@aavar_byntpos, bygene => \%aavar_bygene, bygene2 =>\%aavar_bygene2,
			'nt.byntpos' => \@ntvar_byntpos, 'nt.bygene' => \%ntvar_bygene, 'nt.bygene2' => \%ntvar_bygene2);
	return %vdata;
}

# Filters v4.0 VCF file with a mask specified in a BED file.
#
# Example usage:
# filterVCF($vcf.in, $vcf.out, mask_file.bed)
# mask_file.bed		Bed file with regions to filter
#
sub filterVCF{
	my $vcf_in 	= shift(@_);
	my $vcf_out	= shift(@_);
	my $mask_file	= shift(@_);
		
	# READ MASK BED FILE
	my %mask	= ();
	open(IN,"<$mask_file") or die "Can\'t open $mask_file: $!\n";
	while(my $l=<IN>){
		my ($chrom,$from,$to) = split(/\t/,$l,3);
		$from++; $to++; 	# NOTE: BED file format has 0-based indexing of positions > shift to 1-based
		for(my $pos=$from; $pos<$to; $pos++){	
			$mask{$pos} = 1;
		}
	}	
	
	# READ VCF FILE
	my %header_musthave 	= ("POS" => 1,"REF" => 1, "ALT" => 1,"INFO" => 1,"FORMAT"=> 1);
	my %header_col		= ();
	open(IN,"<$vcf_in") or die "Can\'t open $vcf_in: $!\n";
	open(OUT,">$vcf_out") or die "Can\'t open $vcf_out: $!\n";
	# SKIP METAINFO LINES
	my $l;
	while($l=<IN>){
		print OUT $l;
		if($l =~ m/^#CHROM/i){
			last;
		}
	}
	# READ AND MAP HEADERS
	chomp($l);
	my @sp	= split(/\t/,$l,-1);
	if(scalar(@sp)>10){	# skipping seqid names
		@sp = @sp[0..10];
	}
	for(my $col=0; $col<scalar(@sp); $col++){
		if(defined($header_musthave{$sp[$col]})){
			$header_col{$sp[$col]} = $col;
	}}
	foreach my $h(keys %header_musthave){
		if( !defined($header_col{$h}) ){
			print STDERR "ERROR: parse_aavariants: $vcf_in has no header $h: exiting\n";
			exit(1);
	}}
	
	# READ AND FILTER VARIANT DATA
	while(my $l=<IN>){
        	chomp($l);
		my @sp= split(/\t/,$l,10);	
		my $ntpos 	= $sp[$header_col{'POS'}];
		if(!$mask{$ntpos}){ 
			print OUT $l,"\n";
		}
		
	}
	close(IN);
	close(OUT);
}


# Converts AAVariants ordered by gene into hash into a AAVariantString string representation
#
# my $aavar_str = aavarhash_tostr(%aavar_bygene)
#
# %aavar_bygene		can be %hash->%hash or %hash->@array form
#
sub AAVariantHash2AAVariantString{
	my %gene_vars 	= %{shift(@_)};
	my @genes 	= keys %gene_vars;
	my @varstr_list = ();
	
	
	foreach my $gene( order_genes( \@genes) ){
		my $varstr;
		if(ref($gene_vars{$gene}) eq 'ARRAY'){
		
			my @vars 	= @{$gene_vars{$gene}};
			if(scalar(@vars) == 0){ next; }
			$varstr 	= "$gene: ". join(',', map{ $_->mstr} @vars);
		}
		elsif(ref($gene_vars{$gene}) eq 'HASH'){
			my @vars 	= sort{$a->ntpos <=> $b->ntpos} values %{ $gene_vars{$gene} };
			if(scalar(@vars) == 0){ next; }
			$varstr 	= "$gene: ". join(',', map{ $_->mstr} @vars);
		}
		else{
			$varstr	= '';
		}
		push(@varstr_list, $varstr);
	}
	my $gene_variant_str = join("; ",@varstr_list);
	if($gene_variant_str eq ''){
		$gene_variant_str = ' ';
	}
	return $gene_variant_str;
}


# Converts AAVariantString string to a hash of AAVariants sorted by gene and aa pos
# NOTE: missing info, such as ntpos, ref, alt, count and freq, is set to 'NA' or -1
#
# Example usage:
# my %aavar = AAVariantString2AAVariantHash(my $aavarstr)
#
# $aavar{$gene}->[0]->HVVSp	# retrieve HGVSp for first aa mutation in $gene
#
sub AAVariantString2AAVariantHash{
	my $aavarstr 		= shift(@_);
	my @gene_varstr_list 	= split(/;/,$aavarstr,-1);
	my %aavars_bygene	= ();
	foreach my $gene_varstr(@gene_varstr_list){
		
		
		$gene_varstr 		=~ s/^\s+|\s+$//g;
		my ($gene,$varstr) 	= split(/:/,$gene_varstr,2);
		
		if(!defined($gene) || !defined($varstr)){ next;}
		
		my @aavars	= ();
		
		$varstr =~ s/^\s+|\s+$//g;
		my @variant_list =  split(/,/,$varstr);
		
		foreach my $var(@variant_list){
			$var 		=~ m/^[A-Za-z]*([0-9]+)/i;
			my $aapos 	= $1;
			#my $aavar 	=  AAVariant(-1,$gene,'NA','NA',$aapos,'NA','NA',$var,-1,-1);
			my $aavar 	= AAVariant(ntpos=>-1, gene=>$gene, ref=>'NA',alt=>'NA',
						aapos=>$aapos, HGVSp=>'NA', HGVSc=>'NA', mstr=>$var, count=>-1,freq=>-1); 
			push(@aavars,$aavar);
		}
		if(scalar(@aavars) == 0){ next; }
		
		$aavars_bygene{$gene}	= \@aavars;
	}
	return %aavars_bygene;
}


# my %gene_aavars12 = join_gene_aavariants(\%gene_AAVariants1,\%gene_AAVariants2)
#
# Creates a joint hash from two AAVariant hashes ordered by gene: \%gene_AAVariants1 and \%gene_AAVariants2
sub join_gene_aavariants{
	my $gene_aavar1 = shift;
	my $gene_aavar2 = shift;
	my @hashinp_list = ($gene_aavar1,$gene_aavar2);
	
	my %hash = ();
	
	foreach my $hashinp(@hashinp_list){
	   foreach my $gene(keys %{$hashinp}){
		# init
		if( !defined($hash{$gene}) ){
			my @tmp 	= ();
			$hash{$gene} 	= \@tmp;
		}
		foreach my $var( @{ $hashinp->{$gene} } ){
			push( @{$hash{$gene}}, $var);
		}
	   }
	}
	# sort AAVariants for each gene
	foreach my $gene(keys %hash){
		my @tmp = sort {$a->aapos <=> $b->aapos} @{$hash{$gene}};
		$hash{$gene} = \@tmp;
	}
	
	return %hash;
}

# foreach $gene (order_genes(@genes))
#
# Orders any list of genes. COVID-19 genes are ordere in genetic order, other gene-lists in alphabetical order
#
sub order_genes{
	my @genes_in = @{shift(@_)};
	
	my @pos_list = ("5\'UTR",'ORF1ab','ORF1a','ORF1b','S','ORF3a','ORF3b','E','M','ORF6','ORF7a','ORF7b','ORF8','N','ORF10',"3\'UTR");
	my %pos = ();
	my $i = 0;
	foreach my $gene(@pos_list){ 
		$pos{$gene} = $i;
		$i++;
	}
	
	my @genes_out = sort{ (defined($pos{$a}) && defined($pos{$b})) ? ($pos{$a} <=> $pos{$b}) : ($a cmp $b) } @genes_in;
	return @genes_out;
}



# my $nonempty = print_gaps2bed($file.msa, $file.bed, $chr, $gapmask_coverage_th, $gapmask_gaplen_th)
#
# Creates a BED file representing gaps in the input MSA
#
# $file.msa   		Input MSA file
# $file.bed   		Output BED file
# $chr        		Value for the Chromosome field in the BED file
# $gapmask_coverage_th	Include positions with gap% >= (1 - $gapmask_coverage_th)*100
# $gapmask_gaplen_th	Gap length threshold. Only output gaps with nt length >= $len_th
#
# RETURNS
# $nonempty		Retuns a boolean flag: true if at least one line was printed to the bed file, false otherwise
#
# DEPENDENCIES: trimal
#
sub print_gaps2bed{
	my $msa 	= shift;
	my $bed		= shift;
	my $chr	   	= shift;
	my $cov_th	= shift;
	my $gap_th	= (1-$cov_th)*100;
	my $len_th	= shift;
	my $bed_nonempty= !1;	# flag indicating that there is at least one line in the printed bed file
	
	
	system("trimal -in $msa -out $msa.tmp  -sgc | tail -n+4 1> $msa.gaps"); 	# ignoring first 3 rows

	# filter with gap_th
	open (IN,"<$msa.gaps") or die "Failed to open $msa.gaps: $!\n";
	open (OUT,">$msa.gaps.flt") or die "Failed to ope $msa.gaps.flt: $!\n";
	while(my $l=<IN>){
		chomp($l);
		$l =~ s/^\s*//; 
		my @sp = split(/\t+/,$l,3);
		if($sp[1] >= $gap_th){
			print OUT join("\t",@sp),"\n";
		}
	}
	close(IN);close(OUT);
	
	open (IN,"<$msa.gaps.flt") or die "Failed to open $msa.gaps.flt: $!\n";
	open (OUT,">$bed") or die "Failed to ope $bed: $!\n";
	my @sp;
	my $pos_start;
	my $pos_end;
	my $pos;
	while(my $l=<IN>){
		chomp($l);
		@sp = split(/\t+/,$l,3);
		$pos_start	= $sp[0];
		$pos_end	= $pos_start;
		$pos		= -1;
		last;
	}
	while(my $l=<IN>){
		chomp($l); $l =~ s/^\s*//; 
		@sp = split(/\t+/,$l,2);
		my $pos = $sp[0];
		if($pos > ($pos_end+1)){
			if( ($pos_end-$pos_start+1)>=$len_th){
			print OUT "$chr\t$pos_start\t".($pos_end+1)."\n";
			}
			$pos_start	= $pos;
			$pos_end 	= $pos;
			$bed_nonempty	= 1;
		}
		else{
			$pos_end = $pos;
		}
	}
	if( defined($pos_end) && ( ($pos_end-$pos_start+1)>=$len_th)  ){
		print OUT "$chr\t$pos_start\t".($pos_end+1)."\n";
		$bed_nonempty	= 1;
	}
	close(IN);
	close(OUT);
	system("rm -f $msa.gaps");
	system("rm -f $msa.gaps.flt");
	return ($bed_nonempty);
}

# my %sequences = readfasta(fasta_file)
#
# reads fasta file
# returns hash: seqid_str -> sequence_str
sub readfasta{
  	my $file		= shift(@_);
	my %sequence;
	my $header;
	my $temp_seq;
	
	#suppose fasta files contains multiple sequences;
	 
	open (IN, "<$file") or die "couldn't open the file $file $!";
	
	while (<IN>){	
		chop;
		next if /^\s*$/; #skip empty line 
		if ($_ =~ s/^>//)  #when see head line
		{	
			$header= $_;
			if ($sequence{$header}){print colored("#CAUTION: SAME FASTA HAS BEEN READ MULTIPLE TIMES.\n#CAUTION: PLEASE CHECK FASTA SEQUENCE:$header\n","red")};
			if ($temp_seq) {$temp_seq=""} # If there is alreay sequence in temp_seq, empty the sequence file
			
		}
		else # when see the sequence line 
		{
		   s/\s+//g;
		   $temp_seq .= $_;
		   $sequence{$header}=$temp_seq; #update the contents
		}
	
	}
	
	return %sequence;
}


# SOME SIMPLE STATISTICS

# RETURNS LIST MEDIAN
sub median{
	my @values 	= @{shift(@_)};
	my $mid 	= int @values/2;
	@values 	= sort {$a <=> $b} @values;
	my $median;	
	if (@values % 2) {
    		$median = $values[ $mid ];
	}
	else {
    		$median = ($values[$mid-1] + $values[$mid])/2;
	}
	return $median;
}

sub max{
	my @values 	= @{shift(@_)};
	my $max		= (scalar(@values)>0) ? $values[0] : !1;
	foreach my $val(@values){
		if($val > $max){ $max = $val };
	}
	return $max;
}



sub init_opt{

	# DEFAULT ARGUMENTS
	
	my %color_panels			= ();
	$color_panels{'rgb'}		= ['#FF0000','#00AA00','#0000FF'];
	$color_panels{'paired'}		= ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a']; # https://colorbrewer2.org/: Paired
	$color_panels{'dark2'}		= ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d','#666666']; # https://colorbrewer2.org/: Dark2
	
	my %opt;
	$opt{'pipeline_name'}		= "CovTRace";
	$opt{'refgen'}				= "data/NC_045512.fa";
	$opt{'outgroup'}			= "data/NC_045512.fa";
	$opt{'pangolin_call'}		= "pangolin";
	$opt{'msa_call'}			= "mafft --retree 2 --maxiterate 100";
	$opt{'iqtree_call'}			= "iqtree2 -redo -T AUTO --date TAXNAME"; # Add on request: --mset GTR+F
	$opt{'vftree_call'}			= "VeryFastTree -nt -gtr -nopr";
	$opt{'R_call'}				= "singularity_wrapper exec Rscript --no-save";
	$opt{'vcall_nt_call'}		= "java -jar jars/msa2vcf.jar --haploid";
	$opt{'vcall_aa_call'}		= "java -jar jars/snpEff.jar -no-downstream -no-intergenic -no-upstream -no-utr -hgvs1LetterAa -noStats";
	$opt{'vcall_aa'}			= 1;
	$opt{'vcall_gapmask_coverage_th'}= 0.75;	# regions with lower coverage are masked from variant calling
	$opt{'vcall_gapmask_gaplength_th'}= 30;		# regions with low coverage in stretches longer than this threshold are masked from variant calling
	$opt{'vcall_freq_th_cl'}	= 0.5;		# allele frequency threshold for cluster vcalls
	$opt{'vcall_count_th_cl'}	= 0;		# allele count threshold for cluster vcalls
	$opt{'vcall_freq_th'}		= 0;		# allele freq threshold for lineage vcalls
	$opt{'vcall_count_th'}		= 10;		# allele count threshold for lineage vcalls
	$opt{'vcall_nt_allele_maxlen'}	= 10; 		# nt variants with longer nt length will be excluded, this will not affect aa variants
	$opt{'mutation_table_climit'}	= 10;		# internal switch to limit number of clusters included in the ClusterMutationTable
	$opt{'tree'}				= "iqtree";
	$opt{'ufboot'}				= !1;
	$opt{'trimal_gt'}			= 0.9;	# trimal -gt $opt{'trimal_gt'}
	$opt{'refvar'}				= !1;
	$opt{'minseqn'} 			= 10;
	$opt{'minlen'}				= 90;
	$opt{'maxlen'}				= 110;
	$opt{'maxgap'}				= 10;
	$opt{'pipe'}				= 'all';
	$opt{'target'}				= !1;
	$opt{'numth'}				= 8;
	$opt{'col'}					= '#ff0000';
	$opt{'colpan'}				= 'dark2';
	$opt{'colorclust'}			= 1;		# Internal switch to controll time-demanding tree_colorclust.pl calls
	$opt{'tperiod'}				= "month";
	$opt{'v'}					= !1;
	$opt{'short'}				= !1;
	$opt{'res'}					= "results";
	$opt{'log'} 				= "log";
	$opt{'ref'}					= !1;
	$opt{'keep_wrkdir'}			= !1;	

	# Command line options
	GetOptions(\%opt,
		'fasta=s','res=s','ref=s','log=s','numth|t=i','minseqn=i','minlen=i','maxlen=i','maxgap=i',
		'tree=s','ufboot','trimal_gt=i','outgroup=s','refgen=s','refvar=s','col=s','colpan=s',
		'pipe=s','target=s','short','v','tperiod=s','keep_wrkdir') or die $usage;
	
	if($opt{'ufboot'}){
		$opt{'iqtree_call'} = "$opt{'iqtree_call'} -B 1000 "; # add on request: -bnni
	}

	$opt{'colpan_str'}	= $opt{'colpan'};
	if( !defined($color_panels{$opt{'colpan_str'}})){
		print STDERR "\nInvalid option --colpan $opt{'colpan_str'}\n\n";
		die $usage;
	}
	$opt{'colpan'} 		= $color_panels{$opt{'colpan_str'}};
	unless( ($opt{'tperiod'} eq 'month') || ($opt{'tperiod'} eq 'week')){
		print STDERR "\nInvalid option --tperiod $opt{'tperiod'}\n";
		print STDERR "\nUsing default --tperiod month\n";
		$opt{'tperiod'} = 'month';
	}
	$opt{'refgen_len'}			= get_seqlen($opt{'refgen'});
	$opt{'refgen_id'}			= get_lastseqid($opt{'refgen'});
	$opt{'outgroup_id'} 		= get_lastseqid($opt{'outgroup'});	
	$opt{'lineage_report'} 		= "$opt{'res'}/lineage_report.csv";
	$opt{'vcall_fromnt'}		= 100;		# start vcall from this genomic position
	$opt{'vcall_tont'}			= $opt{'refgen_len'}-100; # end vcall to this genomic position
	

	# PIPELINE STEPS
	my %pipeh;
	my @tmp 	= split(/,/,$opt{'pipe'});
	foreach my $t(@tmp){
		if( lc($t) eq 'p' || (lc($t) =~ m/pango/ig) ){	$pipeh{'pangolin'}	= 1;}
		if( lc($t) eq 'c' || lc($t) eq 'collect'){ 	$pipeh{'collect'}		= 1;}
		if( lc($t) eq 'f' || lc($t) eq 'filter'){	$pipeh{'filter'}		= 1;}
		if( lc($t) eq 'a' || lc($t) eq 'align'){	$pipeh{'align'}			= 1;}
		if( lc($t) eq 't' || lc($t) eq 'tree'){		$pipeh{'tree'}			= 1;}
		if( lc($t) eq 'cl' || lc($t) eq 'clust'){	$pipeh{'clust'}			= 1;}
		if( lc($t) eq 'vc' || lc($t) eq 'vcall'){	$pipeh{'vcall'}			= 1;}
		if( lc($t) eq 'vclineage'){					$pipeh{'vclineage'}		= 1;}
		if( lc($t) eq 'clean'){						$pipeh{'clean'}			= 1;}
		if( lc($t) eq 'pack'){                      $pipeh{'pack'}			= 1;}
		if( lc($t) eq 'all'){
			$pipeh{'pangolin'}	= 1;
			$pipeh{'collect'}	= 1;
			$pipeh{'filter'}	= 1;
			$pipeh{'align'}		= 1;
			$pipeh{'tree'}		= 1;
			$pipeh{'clust'}		= 1;
			$pipeh{'vcall'}		= 1;
			$pipeh{'vclineage'}	= 1;
			$pipeh{'clean'}		= 1;
			$pipeh{'pack'}		= 1;
			last;
		}
	}
	$opt{'pipeh'} = \%pipeh;
	
	# TARGET LINEAGES
	if( $opt{'target'} ){
		my @my_list= split(/,/,$opt{'target'},-1);
		my %my_hash= map { $_ => 1 } @my_list;
		$opt{'target'} = \%my_hash;
	}

	# ARG CHECK
	if( !$opt{'res'} ){
		print STDERR "\nMissing arguments: --res dir\n\n";
		die $usage;
	}
	if( $pipeh{'pangolin'}  && !$opt{'fasta'} ){
		print STDERR "\nMissing arguments: --fasta file\n\n";
		   die $usage;
	}
	if( $pipeh{'collect'}  && !$opt{'fasta'} ){
		print STDERR "\nMissing arguments: --fasta file\n\n";
		   die $usage;
	}
	if( !( ($opt{'tree'} eq 'iqtree') || ($opt{'tree'} eq 'vftree') )  ){
		print STDERR "\nInvalid option --tree $opt{'tree'}\n\n";
		die $usage;
	}
	return %opt;
}
