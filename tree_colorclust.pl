#!/usr/bin/perl
use warnings;
no warnings 'recursion';
use Getopt::Long qw(GetOptions);
use Cwd;
use Bio::TreeIO;

#
# CLUSTER and NODE COLORING IN NEXUS/NEWICK TREES
#
# credit: Ilya Plyusnin, University of Helsinki, Ilja.Pljusnin@helsinki.fi
#

my $usage= 	"\nUSAGE: $0 -t|tree tree[tree2..] -c|clust node_clust.map[map2..] -n|node node_col.map [--monophyl --colpan str] 1> tree.out.nex 2> log\n".
		"\n".
		"-t|tree file[file2..]     : Input tree file in Nexus or Newick format. Assumes to contain a single tree.\n".
		"                          : Specify multiple --tree files to print a multi-tree in nexus format.\n".
		"-c|clust file[file2..]    : Text map assigning nodes to clusters, format <node_id tab cluster_id>\n".
		"                            Specify multiple --clust files to print a multi-tree in nexus format.\n".
		"-n|node file              : Text map assigning nodes to colors, format <node_id tab #[0-F]{6}\n".
		"                            Nodes painted by --clust option will be re-painted\n".
		"--monophyl                : Color only monophyletic clusters, i.e. clusters with no subclusters [false]\n".
		"--colpan                  : Color scheme for cluster coloring: rgb|paired|dark2 [dark2]\n".
		"tree.out.nex              : Output tree file in Nexus format with clusters/nodes colored. Will contain one tree for each --tree --clust pair.\n".
		"\n";

# DEFAULT ARGUMENTS
my %color_panels		= ();
$color_panels{'rgb'}		= ['#FF0000','#00AA00','#0000FF'];
$color_panels{'paired'}		= ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a']; # https://colorbrewer2.org/: Paired
$color_panels{'dark2'}		= ['#1b9e77','#d95f02','#7570b3','#e7298a','#66a61e','#e6ab02','#a6761d','#666666']; # https://colorbrewer2.org/: Dark2

my $PROG_LABEL		= "ClusTRace";
my @tree_files		= ();
my @node_clust_files	= ();
my $node_col_file	= !1;
my $clust_sorted	= !1;
my $monophyl		= !1;
my $colpan		= 'dark2';

GetOptions('tree|t=s{1,}' 	=> \@tree_files, 
	'clust|c=s{1,}' 	=> \@node_clust_files, 
	'node|n=s' 		=> \$node_col_file, 
	'sorted' 		=> \$clust_sorted, 
	'monophyl' 		=> \$monophyl,
	'colpan=s' 		=> \$colpan) or die $usage;
unless( scalar(@tree_files)>0 && (scalar(@node_clust_files)>0 || $node_col_file)){
	print STDERR "\nERROR: missing arguments\n\n";
	die $usage;
}
if( scalar(@node_clust_files)>0 && (scalar(@tree_files) != scalar(@node_clust_files)) ){
	print STDERR "ERROR: there must be exactly one --clust file for each --tree file\n";
	die $usage;
}
my @color_panel		= defined($color_panels{$colpan}) ? @{$color_panels{$colpan}} : @{$color_panels{'dark2'}};


# READING INPUT
my @trees		= ();
my @clust_subclustn	= ();# array of hashes
my @nodeid_clust	= ();# array of hashes
my %nodeid_col		= ();

foreach my $tree_file(@tree_files){
	my $treeio	= Bio::TreeIO->new(-file   => "$tree_file");
	my $tree	= $treeio->next_tree();
	bootstrap_values_fromids($tree);
	push(@trees,$tree);
}

foreach my $node_clust_file(@node_clust_files){
	my %hash1= ();
	my %hash2= ();
	read_tohash($node_clust_file,0,1,\%hash1);	# one to one map
	read_tohash($node_clust_file,1,2,\%hash2);
	push(@nodeid_clust,\%hash1);
	push(@clust_subclustn,\%hash2);
}
if( $node_col_file ){
	read_tohash($node_col_file,0,1,\%nodeid_col);
}



# ADDING COLORS TO CLUSTERS
for( my $i=0; $i< scalar(@trees); $i++){

    if( scalar(@nodeid_clust)>0){
    
	my %clust_lca = get_cluster_lca($trees[$i], $nodeid_clust[$i]);
	my @clust_order = sort {$a <=> $b} keys %clust_lca;

	# add color to all clustered nodes (not to singletons)
	foreach my $clust(@clust_order){
		if($monophyl && ($clust_subclustn[$i]->{$clust} > 0)){
			#print STDERR "paraphyletic\n";
			next;
		}
		my $lca 	= $clust_lca{$clust};
		my @clade_nodes = ($lca, $lca->get_all_Descendents());
		foreach(@clade_nodes){
			$_->set_tag_value("!color", $color_panel[ ($clust % scalar(@color_panel)) -1]);
		}
	}
	
	# ADDING CLUSTER IDS TO LEAVES
	my @leaf_nodes	= $trees[$i]->get_leaf_nodes();
	foreach my $node(@leaf_nodes){
		my $id = $node->id();
		if( defined($nodeid_clust[$i]->{$id}) ){
			my $cl	= $nodeid_clust[$i]->{$id};
			#$node->id( "$id.C$cl" );
			$node->set_tag_value("cluster",$cl);
			$node->set_tag_value("id_cluster","$id.C$cl");
		}
		else{
			#$node->id( "$id.C-1" );
			$node->set_tag_value("cluster",-1);
			$node->set_tag_value("id_cluster","$id.C-1");
		}
	}
    }	
}

# ADDING COLORS TO NODES
for( my $i=0; $i < scalar(@trees); $i++){

   if( scalar(keys %nodeid_col)>0 ){
	foreach my $nodeid(keys %nodeid_col){
		my $node = $trees[$i]->find_node($nodeid);
		unless(defined($node)){
			#print STDERR "WARNING: unknown nodeid=$nodeid in $node_col_file\n";
			next;
		}
		$node->set_tag_value("!color", $nodeid_col{$nodeid});
	}
    }
}

if(scalar(@trees)>1){
	print_trees_nexus(*STDOUT,\@trees);
}
else{
	print_tree_nexus(*STDOUT,$trees[0]);
}















# read_tohash(file,key_ind,val_ind,\%hash)
# 
# Does not support multiple values for a single key. The last value encountered is saved.
sub read_tohash{
	my $file= shift;
	my $keyi= shift;
	my $vali= shift;
	my $hashp= shift;
	#print STDERR "# read_tohash from $file\n";
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
        	$ln++;
		if($l =~ m/^[@#!]/){ next; }
		chomp($l);
		my @sp= split(/\t/,$l,-1);
        	$hashp->{$sp[$keyi]} = $sp[$vali];
	}
	close(IN);
}

# bootstrap_values_fromids(Bio::Tree::TreeI)
#
# Iqtree will print bootstrap support values as inner node ids.
# Here we copy these values to tag "bootstrap"
sub bootstrap_values_fromids{
	my $tree = shift();
	my @nodes = $tree->get_nodes();
	foreach(@nodes){
		if($_->is_Leaf()){next;}
		if(defined($_->id)){
			$_->set_tag_value("bootstrap",$_->id);
		}
	}
}


# print_tree_nexus(OUT, tree)
#
# OUT		file handle to print to
# tree		Bio::Tree::TreeI
sub print_tree_nexus{
	my $FOUT 	= $_[0];
	my $tree 	= $_[1];
	my @leaf_nodes	= $tree->get_leaf_nodes();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time); $year += 1900;
	my $TIME_LABEL 	= sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year, $mon, $mday, $hour, $min, $sec); 
	
	print $FOUT "#NEXUS [Created by $PROG_LABEL $TIME_LABEL]\n";
	
	print $FOUT "\nbegin taxa;\n";
	print $FOUT "\tdimensions ntax=",scalar(@leaf_nodes),";\n";
	print $FOUT "\ttaxlabels\n";
	foreach my $node(@leaf_nodes){
		print $FOUT "\t",($node->id());

		if( scalar($node->get_all_tags()) > 0 ){
			my $tag_val_str	= join(",", map { "$_=".(($node->get_tag_values($_))[0]) } $node->get_all_tags() );
			print $FOUT "[&$tag_val_str]";
		}
		print $FOUT "\n";
	}
	print $FOUT ";end;\n";
	
	print $FOUT "\nbegin trees;\n";
	print $FOUT "\ttree tree_1 = ";
	print $FOUT get_newick_str($tree->get_root_node()),";\n";
	#print_newick($FOUT,$tree->get_root_node());
	print $FOUT "end;\n";
}


# print_trees_nexus(OUT, \@trees)
#
# OUT		file handle to print to
# @tree		Bio::Tree::TreeI array
sub print_trees_nexus{
	my $FOUT 	= $_[0];
	my @trees 	= @{$_[1]};
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time); $year += 1900;
	my $TIME_LABEL 	= sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year, $mon, $mday, $hour, $min, $sec); 
	
	# collect leaves from all trees, for identical nodes the last encounter (last tree) is used
	my %leaf_nodes	= ();
	foreach my $tree(@trees){
		my @leaves	= $tree->get_leaf_nodes();
		foreach my $node(@leaves){
			my $id	= $node->id();
			$leaf_nodes{$id} = $node;
		}
	}
	
	print $FOUT "#NEXUS [Created by $PROG_LABEL $TIME_LABEL]\n";
	
	print $FOUT "\nbegin taxa;\n";
	print $FOUT "\tdimensions ntax=",scalar(keys %leaf_nodes),";\n";
	print $FOUT "\ttaxlabels\n";
	foreach my $id( sort keys %leaf_nodes ){
		my $node = $leaf_nodes{$id};
		print $FOUT "\t",($node->id());
		
		# WE DONT PRINT TAGS FOR MULTI-TREE NEXUS, SINCE TAGS CAN DIFFER BETWEEN TREES
		#if( scalar($node->get_all_tags()) > 0 ){
		#	my $tag_val_str	= join(",", map { "$_=".(($node->get_tag_values($_))[0]) } $node->get_all_tags() );
		#	print $FOUT "[&$tag_val_str]";
		#}
		print $FOUT "\n";
	}
	print $FOUT ";end;\n";
	
	print $FOUT "\nbegin trees;\n";
	for(my $i=0; $i<scalar(@trees); $i++){
		print $FOUT "\n";
		print $FOUT "\ttree tree_$i = ";
		print $FOUT get_newick_str($trees[$i]->get_root_node()),";\n";
		#print_newick($FOUT,$trees[$i]->get_root_node());
	}
	print $FOUT "\nend;\n";
}




# $node_str = get_newick_str( $node )
#
# $node		Bio::Tree::NodeI
# $node_str	Subtree rooted in $node in Newick format
#
# e.g. to get a Newick string representation of the whole tree:
# $tree_newick_str = get_newick_str( $tree->get_root_node() )
#
sub get_newick_str{
	my $node 	= shift(@_);
	my $node_str	= "";
	# Inner nodes: children
	if( !($node->is_Leaf()) ){
		$node_str .= "(";
		$node_str .= join(",", map{ get_newick_str($_) } $node->each_Descendent() );
		$node_str .= ")";
	}
	# Inner and leaf nodes: tags
	if( scalar($node->get_all_tags()) > 0 ){
		my $tag_val_str	= join(",", map { "$_=".(($node->get_tag_values($_))[0]) } $node->get_all_tags() );
		$node_str .= "[&$tag_val_str]";
	}
	# Inner and leaf nodes: id and branch length
	$node_str .= defined($node->id)? $node->id: '';
	$node_str .= defined($node->branch_length)? ":".$node->branch_length() : '';		
	return $node_str;
}

# print_newick( FOUT, $node )
#
# FOUT		output file handle to print to
# $node		Bio::Tree::NodeI
#
# e.g. to print a Newick string representation of the whole tree to STDOUT:
# print_newick( *STDOUT, $tree->get_root_node() )
#
sub print_newick{
	my $FOUT	= shift(@_);
	my $node 	= shift(@_);
	# Inner nodes: children
	if( !($node->is_Leaf()) ){
		print $FOUT "(";
		my @child_list = $node->each_Descendent();
		if(scalar(@child_list)>0){
			print_newick($FOUT,shift(@child_list));
		}
		foreach my $child(@child_list){
			print $FOUT ",";
			print_newick($FOUT,$child);
		}
		print $FOUT ")";
	}
	# Inner and leaf nodes: tags
	if( scalar($node->get_all_tags()) > 0 ){
		my $tag_val_str	= join(",", map { "$_=".(($node->get_tag_values($_))[0]) } $node->get_all_tags() );
		print $FOUT "[&$tag_val_str]";
	}
	# Inner and leaf nodes: id and branch length
	print $FOUT (defined($node->id)? $node->id: '');
	print $FOUT (defined($node->branch_length)? ":".$node->branch_length() : '');
}

# my %clust_lca = get_cluster_lca($tree, \%nodeid_clustid)
# $tree			Bio::Tree::TreeI
# \%nodeid_clustid	nodeid -> clustid map
# %{$clust_lca}		clustid->lca hash
#
# Retrieves a list of Lowest Common Ancestors (LCAs) for each cluster in the nodeid_clustid hash.
#
sub get_cluster_lca{
	my $tree		= shift(@_);
	my %nodeid_clust	= %{shift(@_)};

	my %clust_nodes	= ();
	foreach my $nodeid(keys %nodeid_clust){
		my $clust = $nodeid_clust{$nodeid};
		if(!defined($clust_nodes{$clust})){
			my @tmp; $clust_nodes{$clust} = \@tmp;
		}
		my $node = $tree->find_node($nodeid);
		unless(defined($node)){
			print STDERR "WARNING: unknown nodeid=$nodeid\n";
			next;
		}
		push(@{$clust_nodes{$clust}},$node);
	}
	
	# representing clusters by LCA
	my %clust_lca 	= ();
	foreach my $clust(sort keys %clust_nodes){
		if($clust == -1){ next; }	# skip singletons
		if(scalar(@{$clust_nodes{$clust}}) < 2){
			next;
		}
		$clust_lca{$clust} = $tree->get_lca( @{$clust_nodes{$clust}} );
	}

	return %clust_lca;
}
