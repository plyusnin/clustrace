#!/usr/bin/perl
use warnings;
#use Getopt::Long qw(GetOptions);

#
# Creates Variant Table from a collection of Variant files.
#
# credit: Ilya Plyusnin, University of Helsinki, Ilja.Pljusnin@helsinki.fi


my $usage = "USAGE: $0 dir 1> vartable.txt\n".
		"\n".
		"dir   : Directory with variant file(s), format:\n".
		"        #Lineage=lineage_id\n".
		"        #gene \\t aminoacid \\n\n".
		"        gene_x \\t variant_y\n".
		"NOTE  : By default, will convert ORF1a+ORF1b to ORF1ab using 4401 offset\n".
		"        To keep ORF1a/ORF1b deselect option in the code\n".
		"\n";

# OPTIONS
my $convert_orf1ab	= 1;

if(scalar(@ARGV)<1){
	die $usage;
}
my $indir 	= $ARGV[0];
my @files	= <$indir/*.tab>;
push(@files, <$indir/*.txt>);

foreach my $file(@files){
	#print STDERR "# Reading $file..\n";
	
	my %gene_variant_pos = read_gene_variant_pos($file);
	
	# START: CONVERT ORF1a and ORF1b to ORF1ab
	if( $convert_orf1ab ){
		my %ORFab = ();
		if( defined($gene_variant_pos{'ORF1a'})){
			%ORFab = %{$gene_variant_pos{'ORF1a'}};
			delete($gene_variant_pos{'ORF1a'});
		}
		if( defined($gene_variant_pos{'ORF1b'})){
			my %ORF1b = %{$gene_variant_pos{'ORF1b'}};
			foreach my $variant(keys %ORF1b){
				$variant =~ s/([0-9]+)/$1+4401/eg;
				$variant =~ m/([0-9]+)/;
				$ORFab{$variant} = $1;
			}	
			delete($gene_variant_pos{'ORF1b'});
		}
		$gene_variant_pos{'ORF1ab'}= \%ORFab;
	}
	# END 
	
	my $gene_variant_str = vcall_aa_hash_tostr(\%gene_variant_pos);
	my %metadata	= read_keyvalues_fromcomments($file,'=');
	
	if(!defined($metadata{'lineage'})){
		print STDERR "ERROR: no \'lineage\' specified in $file: skipping\n";
		next;
	}
	
	print "$metadata{'lineage'}\t$gene_variant_str\n";
}



# my %gene_variant_pos = vcall_aa_str_tohash("gene1: A10R,A20N; gene2: E10Q,E20G")
#
# Parses vcall for aminoacids to a hash struct
# Example usage:
# $gene_variant_pos->{$gene}->{$variant} # retrieves position for $variant in $gene
#
sub vcall_aa_str_tohash{
	my $vcall_aa_str = shift(@_);
	
	my @gene_varstr_list = split(/;/,$vcall_aa_str);
	my %gene_variant_pos = ();
	foreach my $gene_varstr(@gene_varstr_list){
		$gene_varstr =~ s/^\s+|\s+$//g;
		my ($gene,$varstr) = split(/:/,$gene_varstr,2);
		if(!defined($gene) || !defined($varstr)){ next;}
		$varstr =~ s/^\s+|\s+$//g;
		my @variant_list =  split(/,/,$varstr);
		my %variant_pos	 = ();
		foreach my $var(@variant_list){
			$var =~ m/[A-Z]+([0-9]+)/gi;
			my $pos = $1;
			$variant_pos{$var} = $pos;
		}
		if(scalar(%variant_pos) == 0){ next; }
		$gene_variant_pos{$gene} = \%variant_pos;
	}
	return %gene_variant_pos;
}

# my $vcall_str = vcall_aa_hash_tostr(\%gene_variant_pos)
#
# if there no variants in the hash, returns ' '
sub vcall_aa_hash_tostr{
	my %gene_variant_pos = %{shift(@_)};
	my @variant_str_list = ();
	foreach my $gene(sort keys %gene_variant_pos){
		my %variant_pos = %{$gene_variant_pos{$gene}};
		if(scalar(%variant_pos) == 0){next;}
		my $variant_str = "$gene: ". join(",",sort {$variant_pos{$a} <=> $variant_pos{$b}} keys %variant_pos);
		push(@variant_str_list,$variant_str);
	}
	my $gene_variant_str = join("; ",@variant_str_list);
	if($gene_variant_str eq ''){
		$gene_variant_str = ' ';
	}
	return $gene_variant_str;
}


# my $HGVS_short = format_vcall($HGVS)
#
# Formats vcalls, generally returning a shorter version:
#
# - Converts 3-letter aa-codes in variant string (HGVS format) to 1-letter aa-codes
# - Converts deletions:  Phe168_Glu169del >> del168/169
# 
sub vcall_toshort{

my %map_tri_mono_hash = (
	'ALA' => 'A',
	'ARG' => 'R',
	'ASN' => 'N',
	'ASP' => 'D',
	'CYS' => 'C',
	'GLU' => 'E',
	'GLN' => 'Q',
	'GLY' => 'G',
	'HIS' => 'H',
	'ILE' => 'I',
	'LEU' => 'L',
	'LYS' => 'K',
	'MET' => 'M',
	'PHE' => 'F',
	'PRO' => 'P',
	'SER' => 'S',
	'THR' => 'T',
	'TRP' => 'W',
	'TYR' => 'Y',
	'VAL' => 'V');

	my $HGVS = shift(@_);
	
	my $aa_pref = uc(substr($HGVS,0,3));
	my $aa_suff = uc(substr($HGVS,-3,3));
	
	## CONVERT AA-TRIPLETS TO A SINGLE CHAR
	# case1: substitution, will have aa-prefix and suffix: eg ALA123ARG
	if( defined($map_tri_mono_hash{$aa_pref})  && defined($map_tri_mono_hash{$aa_suff}) ) {
		substr($HGVS,0,3,$map_tri_mono_hash{$aa_pref});
		substr($HGVS,-3,3,$map_tri_mono_hash{$aa_suff});
	}
	# case2: deletions, insertion and other calls: screen for aa triple-codes
	else{
		for(my $pos= 0; $pos <=(length($HGVS)-3); $pos++){
			my $tri = uc(substr($HGVS,$pos,3));
			if( defined( $map_tri_mono_hash{$tri} ) ){
				substr($HGVS,$pos,3,$map_tri_mono_hash{$tri});
			}
		}
	}
	
	## CONVERT DELETION FORMAT
	if( $HGVS =~ /^[A-Z]{1}([0-9]+)_[A-Z]{1}([0-9]+)del$/i){
		$HGVS = "del$1/$2";
	}
	
	return $HGVS;
}

# read_gene_variant_pos(file)
#
# Reads %gene_variant_pos hash structure from a two-column text file (format: gene \t variant \n)
# Retearned hash includes a hash for each gene with variant position as values for the subhash
#
# Example usage:
# my %gene_variant_pos = read_gene_variant_pos(file,gene_ind,variant_ind)
#
# my %variant_pos = %{$gene_variant_pos{'ORF1ab'}}	# get sub-hash for gene ORF1ab with variants as keys and variant positions as values
# my @ORF1ab_variants = sort {$variant_pos{$a} <=> $variant_pos{$b}} keys %variant_pos;	# get variants for ORF1ab sorted by position
#
sub read_gene_variant_pos{
	my $file	= shift;
	my %hash= ();
	
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
        	$ln++;
		if($l =~ m/^[@#!]/){ next; }
		chomp($l);
		my ($gene,$variant) =  split(/\t/,$l,2);
		if( !defined($gene) || !defined($variant)){next;}
		
		my $pos = 0;
		if($variant =~ /([0-9]+)/g){
			$pos =  $1;
		}
		
		if( !defined($hash{$gene}) ){
			my %new = ();
        		$hash{$gene} = \%new;
		}
		$hash{$gene}->{$variant} = $pos;
	}
	close(IN);
	return %hash;
}

# read_keyvalues_fromcomments(file, separator)
#
# Reads key-value pairs from commented part at the top of the file
# "#key=value\n", where key/value separator can be '=' or any other
# keys are converted to lowercase
#
# Example usage
# my %values = read_keyvalues_fromcomments($myfile, '=')
# print $values{'date'}
# print $values{'lineage'}
#
sub read_keyvalues_fromcomments{
	my $file = shift;
	my $sep	 = shift;
	my %hash = ();
	
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	while(my $l=<IN>){
        	
		if($l =~ s/^[#!]//){
			chomp($l);
			my($key,$val) = split(/$sep/,$l,2);
			#print "DEBUG: $key,$val\n";
			if(!defined($key) || !defined($val)){next;}
			$key =~ s/^\s+|\s+$//g;
			$val =~ s/^\s+|\s+$//g;
			$hash{lc($key)}= $val;
		}
		else{
			last;
		}
	}
	close(IN);
	return %hash;
}
