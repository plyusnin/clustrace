# README #

PIPELINE FOR ANALYZING CLUSTERS/CLADES IN VIRUS PHYLOGENIES

## What is ClusTRace? ##

ClusTRace is a bioinformatic pipeline for high level handling and phylogenetic/cluster analysis of virus sequences.  
ClusTRace was created as an aid for COVID-19 transmission chain tracing.

![ClusTRace flowchart](./img//Pipeline-Flowchart-B.png)


### ClusTRace supports:

* assigning lineages to consensus sequences with Pangolin
* collecting consensus sequences into multi-fasta files according to lineage
* filtering outlier sequences
* creating multiple sequence alignments (MSA)
* creating phylogenetic trees from MSAs
* cluster analysis
    * extracting sequence clusters from the obtained phylogenetic trees
    * extracting clusters at different mutation rates and with different methods supported in TreeCluster
    * visualising clusters with different colors/labels in phylogenetic trees
    * summarizing clusters with excel tables, that depict cluster size, sequence composition, growth rate and support information 
* variant calling
    * calling nucleotide variants for lineage and/or cluster MSA(s)
    * calling amino acid variants for lineage and/or cluster VCF(s)
    * summarizing nucleotide and amino acid variants with data tables
    * visualizing lineage amino acid variants with interactive lollipop graphs (g3viz)
	
#### Supported environments

* Linux (multiple cores recommended)


## How to install ClusTRace? ##

### Clone the repository ###
    
    git clone https://plyusnin@bitbucket.org/plyusnin/clustrace.git
	cd clustrace

### Install Bioinformatic tools ###

#### Install binaries with Conda ####

    conda create -n clustrace -c bioconda iqtree mafft jvarkit-msa2vcf pangolin seqkit snpeff treecluster trimal veryfasttree
    conda activate clustrace
	
Or install from yaml file

    conda env create -f clustrace.env.yml
	conda activate clustrace

#### Install Perl Modules ####

Install perl modules to ~/perl5 (or choose another directory according to your settings):
 
    cpanm --local-lib=~/perl5 Clone Excel::Writer::XLSX File::Temp Getopt::Long Sort::Naturally Struct::Dumb Time::Piece
	# install BioPerl
	cpanm -i -f --notest --local-lib=~/perl5 Bio::TreeIO 
    #add this line to your ~/.bashrc file
    export PERL5LIB=~/perl5/lib/perl5

#### Complete list of dependencies ####

| Tool           | CSC module      | Website                                   | Direct download            | Original article                      |
| :------------- | :-------------- |:----------------------------------------  | :------------------------- |-------------------------------------- |
| IQ-TREE 2      | NA              | <http://www.iqtree.org/>                    | [linux 64-bit](https://github.com/Cibiv/IQ-TREE/releases/download/v1.6.12/iqtree-1.6.12-Linux.tar.gz) | https://doi.org/10.1093/molbev/msaa015 |
| MAFFT v7       | biokit          | <https://mafft.cbrc.jp/alignment/software/> | [linux 64-bit](https://mafft.cbrc.jp/alignment/software/mafft-7.475-linux.tgz) | https://doi.org/10.1093/molbev/mst010 |
| MsaToVcf       | NA              | <http://lindenb.github.io/jvarkit/MsaToVcf.html> | NA                  | https://doi.org/10.1099/mgen.0.000056
| Pangolin       | bioconda        | <https://cov-lineages.org/resources/pangolin.html> | NA                | https://doi.org/10.1093/ve/veab064 |
| SeqKit         | NA              | <https://bioinf.shenwei.me/seqkit/>         | [linux 64-bit](https://github.com/shenwei356/seqkit/releases/download/v0.16.0/seqkit_linux_amd64.tar.gz) | https://doi.org/10.1371/journal.pone.0163962 |
| snpEff         | NA              | <http://pcingola.github.io/SnpEff/>       | NA                         | https://doi.org/10.4161/fly.19695
| TreeCluster    | NA              | <https://github.com/niemasd/TreeCluster>   | NA                        | https://doi.org/10.1371/journal.pone.0221068 |
| Trimal         | biokit          | <http://trimal.cgenomics.org/>             | [linux 64-bit](http://trimal.cgenomics.org/_media/trimal.v1.2rev59.tar.gz) | https://doi.org/10.1093/bioinformatics/btp348 |
| VeryFastTree | NA              | <https://github.com/citiususc/veryfasttree> | NA                         | https://doi.org/10.1093/bioinformatics/btaa582 |
| PERL MODULES |||||
| Bio::TreeIO  | biokit          | <https://metacpan.org/pod/Bio::TreeIO>          |||
| Excel::Writer::XLSX | NA       | <https://metacpan.org/pod/Excel::Writer::XLSX>  |||
| File::Temp          | NA       | <https://metacpan.org/pod/File::Temp>           |||
| Getopt::Long        | NA       | <https://metacpan.org/pod/Getopt::Long>         |||
| Sort::Naturally     | NA       | <https://metacpan.org/pod/Sort::Naturally>      |||
| Struct::Dumb        | NA       | <https://metacpan.org/pod/Struct::Dumb>         |||
| Time::Piece         | NA       | <https://metacpan.org/pod/Time::Piece>          |||

Table1: ClusTRace dependencies


## Running ClusTRace ##

Test ClusTRace and print options:
    
    perl clustrace.pl
	
Test ClusTRace by running all steps on sample data:

    perl clustrace.pl --fasta data/samples/delta-s1.fasta --res results/delta-s1 --pipe all -t 16 -v 
    
Assign lineage to fasta sequences with Pangolin:

    perl clustrace.pl --fasta file --res results/January --pipe pangolin
    
Collect consensus sequences to multi-fasta files by assigned lineage (--res dir must contain lineage_report.csv). Target analysis to Alfa and Beta variants of concern (VOC):

    perl clustrace.pl --fasta file --res results/January --pipe collect --target B.1.1.7,B.1.351
    
Analyse collected multi-fasta: remove outliers + create MSAs + create trees

    perl clustrace.pl --res results/January --pipe filter,align,tree

Control outlier filtering: filter my seqlength 5% deviation from median + by >10% gaps
    
    perl clustrace.pl --res results/January --minlen 95 --maxlen 105 --maxgap 10 --pipe f,a,t
    
Extract clusters for Alfa variant with TreeCluster. Clusters will be extracted with max-clade method at different max mutation rates. Pipeline will also create summary Excel table with cluster statistics and growth rates, and Nexus trees with clusters identified by node color and label.

    perl clustrace.pl --res results/January --pipe cl --tperiod week --target B.1.1.7
    
Create cluster MSA(s) , VCF files (Variant Call Format files) and VCF summaries. These will include both nucleotide and amino acid variants.

    perl clustrace.pl --res results/January --pipe vcall --target B.1.1.7 --refvar data/lineage_variants.tab
    
Create lineage VCF files and summaries.

    perl clustrace.pl --res results/January --pipe vclineage --refvar data/lineage_variants.tab
     
Cleenup temporary files and pack results to a tarball:

    perl clustrace.pl --res results/January --pipe cleen,pack

    
**ClusTRace command line options**

| Option            | Value | \[Default\] | Function  |
|:----------------- |:------|:----------- |:--------- |
| --fasta           | file  |             | Input multifasta (\*.fa or \*.fasta) |
| --res             | dir   | results     | Output directory  |
| --log             | dir   | log         | Directory for logging  |
| --colpan          | str   | dark2       | Color scheme for coloring clusters: rgb\|paired\|dark2 (for preview see [https://colorbrewer2.org/]) |
| --minseqn         | int   | 10          | Lineage filtering: exclude lineages with seqn<minseqn |
| --minlen          | int   | 90          | Sequence outlier filtering: exclude sequences shorted than median_length\*minlen% |
| --maxlen          | int   | 110         | Sequence outlier filtering: exclude sequences longer than median_length\*maxlen% |
| --maxgap          | int   | 10          | Sequence outlier filtering: exclude sequences with gaps% > maxgap% |
| --tree            | str   | iqtree      | Run iqtree (IQ-Tree2 --mset GTR+F) or vftree (VeryFastTree --gtr -nt) |
| --ufboot          |       | false       | Run iqtree with ultrafast bootstrap and create consensus tree (IQ-Tree2 -B 1000 -bnni) |
| --trimal_gt       | num   | 0.9         | trimal -gt threshold. Used to trim MSAs before tree construction.  |
| --tperiod         | str   | month       | Time period for cluster analysis. Accepted values: month\|week |
| --outgroup        | file  | data/NC_045512.fa |  Fasta with outgroup sequence |
| --refgen          | file  | data/NC_045512.fa |  Fasta with reference genome  |
| --refvar          | file  |             |  File with reference lineage variants. Format: lineageid \t gene1: var1,var2,..\[,varn\]; gene2: var1,var2,..\[,varn\] \n. GISAID characteristic mutations for some SARS-CoV-2 lineages are available in data/lineage_variants.tab
| --pipe            | str   | p,c,f,a,t    | Comma-separated list of steps to perform, eg --pipe p,c,f,a |
|           | p\|pangolin|             | Assign lineages with Pangolin. Lineage report is printed to --res dir 
|           | c\|collect |             | Collect sequences for each lineage into multi-fasta
|           | f\|filter  |             | Filter lineage multi-fasta
|           | a\|align   |             | Create MSAs for each lineage multifasta in --res dir
|           | t\|tree    |             | Create ML-trees for each lineage MSA in --res dir. Use --ufboot option to create concensus trees.
|           | cl\|clust  |             | Extract clusters at various mutation rates
|           | vc\|vcall  |             | Create MSA and VCF files for all clusters. Add vcf variants to cluster summary excel.
|           | vclineage  |             | Create VCF files for all lineage MSAs in --res dir. Create excel summary with VCF variants for each lineage.
|           | pack       |             | Pack results into a tarball. Tarball will be created to the root directory of --res dir.
|           | cleen      |             | Cleen up space by removing all intermediate and temporary files.
|           | all        |             | Run all steps
| --target  | str   | false       | Comma-separated list of target lineages to analyze (eg --target B.1.1.7). When omitted, will analyze all lineages 
| --numth   | int   | 8           | Number of threads
| --short   |       | true        | Truncate sequence names to the first occurrence of "\_"
| -v        |       | false       | Run in verbal mode |

   
	
## What is the output of ClusTRace? ##

Results will be printed to --res dir.

| File                      | --pipe step  | Description |
|:------------------------- |:-------------|:------------|
lineage.fa                  | collect      | Multi-fasta with sequences for each lineage. After running "--pipe filter" outliers are excluded from these files.
lineage.fa.flt              | filter       | Multi-fasta with filtered (i.e. excluded) sequences for each lineage
lineage.fa.stats            | filter       | Statistics (length, gap content, ..) and applied filters for sequences in each lineage (tab-delimited format).
lineage.msa                 | align        | MSA for each analysed lineage/multifasta
lineage.ml.tree             | tree         | Maximum likelihood tree for each analysed lineage/multifasta, newick format.
lineage.con.tree            | tree         | Bootstrap consensus tree for each analysed lineage/multifasta, newick format. Required options: --tree iqtree --ufboot
lineage.mr=x.nex            | clust        | Clusters for consensus (or ml) tree at mutation rate x highlighted in different colors, nexus tree file
lineage.cl.xlsx             | clust        | Clusters for consensus (or ml) tree at different mutation rates, Excel table
lineage.cluster_summary.xlsx| clust        | Cluster summary for each lineage. Includes data sheets clustSeqN and clustSeqID, and summary sheets clustGR_MR=X, clustMutations_MR=X and clustMutationTable_MR=X.
    sheet: clustSeqN         | clust        | reports the number of sequences in each cluster for each time period 
    sheet: clustSeqID        | clust        | lists sequence ids assigned to each cluster at each time period
    sheet: clustGR_MR=X      | clust        | reports cluster size, median and max growth rates and support values
    sheet: clustMutations_MR=X| vcall       | reports nt and aa mutations for each cluster, reference aa mutations and non-refenrece aa mutations. Reporting non-reference mutations requires option --refvar file. 
    sheet: clustMutationTable_MR=X| vcall   | reports aa mutations for the 10 fastest growing clusters in a binary matrix. Top row lists aa mutations in genomic order with non-reference mutations highlighted in bold.
|                                |         | LEGEND: "period", date period (data from the first date to this date), "mr", mutation rate, "cluster", cluster id, "seqn", number of sequences assigned to this cluster, "subclustern", number of subclusters for this cluster, "support", bootstrap support
lineage.vcf                 | vclineage    | Variant Call Format file (VCF) with nt and aa variants for each analysed lineage
lineageSummary.xlsx         | vclineage    | Variant summary for analysed lineaged.
    sheet: lineageMutations | vclineage    | reports nt and aa mutataions for each lineage, reference aa mutations and non-reference aa mutations. Reporting non-reference mutations requires option --refvar file.


## Creating interactive mutation plots with g3viz

Interactive aa mutation plots can be created using ClusTRace interface to [g3viz R library](https://github.com/G3viz/g3viz).

The function requires as input the target genome gff-file, vcf-file, g3viz chart.options and reference aa variants (optional).

Example usage:
    
    library(g3viz)
    source("ClusTRace.R")
    
    chart.options <- g3Lollipop.theme(theme.name = "default",title.text = "AA variants for B.1.351")
    chart.options$titleFont             = "bold 18px Arial"
    chart.options$chartWidth            = 1500;
    chart.options$lollipopTrackHeight   = 500;
    chart.options$lollipopPopMinSize    = 8;
    ref.var.B.1.351                     = "ORF1ab: T265I,K1655N,K3353R,del3675/3677,P4715L; ORF3a: Q57H,S171L; S: D80A,D215G,del241/243,K417N,E484K,N501Y,D614G,A701V; E: P71L; N: T205I;"
    viralLollipop( gff.file=data/NC_045512.gff,
                   vcf.file=B.1.351.vcf,
                   chart.options=chart.options,
                   ref.var=ref.var.B.1.351, ac.min=10,fromnt=100, tont=29000)



## Linking in-house data to the pipeline trees

In-house data can be exported from an excel file and displayed in any nexus or newick tree using a simple procedure:

1. Start by setting the first column in your excel sheet to the sequence/sample label displayed in the tree (displayed as ”Tip Labels” in FigTree). Set the header name of the first column to ”taxa”.
2. Export your excel sheet to a tab-delimited annotation file. For this select ”File->Export->Change File Type”. Select ”Text (Tab delimited)” and click ”Save As”.
3. Open any tree you wish to import to with FigTree. Select ”File -> Import Annotations”, select your exported annotation file and click ”Open”.
4. To view imported annotations: from FigTree menu select ”Tip Labels” and from the ”Display” drop-down menu select the field you wish to display (e.g. ”location”).

## Citing ClusTRace

Plyusnin, I., Truong Nguyen, P.T., Sironen, T. et al. ClusTRace, a bioinformatic pipeline for analyzing clusters in virus phylogenies. BMC Bioinformatics 23, 196 (2022). 
[https://doi.org/10.1186/s12859-022-04709-8]

## Who do I talk to? ##

* For further enquiries please contact our development team: group-clustrace(a)helsinki.fi
* More info is also available at [ClusTRace website](https://www2.helsinki.fi/en/projects/clustrace/) 
